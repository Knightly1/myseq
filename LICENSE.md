# MySEQ Open
(where it all began...)

Welcome to MySEQ Open! The original version of MySEQ.
MySEQ Open 2.3.1, Copyright (c) 2003-2013 ShowEQ Project Developers

Smart EQ Offset Finder - GPL Edition
Copyright 2007-2009, Carpathian <Carpathian01@gmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

If you would like to read more about the GNU
General Public License, please visit
http://www.gnu.org/licenses/licenses.html

If you would like to visit the MySEQ Open Forums,
we are located at
http://www.showeq.net/forums/forumdisplay.php?60-MySEQ

Keep it free. Keep it fair. Keep it fun.
