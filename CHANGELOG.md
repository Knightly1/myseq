# MySEQ Release Notes

## 2021-06-03
- Client
  - Previously the default key combination for characters 11 and 12 were CTRL+SHIFT+B and CTRL+SHIFT+C respectively.  This has been updated to be (CTRL+SHIFT)Minus and Plus.
  - Added showing of up to 54 characters (#5) and paved the way for this to be dynamic in the future

## 2021-06-02
- Client
  - Add support for level 115
  - Update Races
  - Add 127.0.0.1 to the network interface list if it isn't automatically picked up

## 2021-05-31

- Client
  - Increased port range from artificial limitation of 32767, all server ports now work in the client
  - Removed dependency on SpeechLib in favor of System.Speech
  - Updated Weifenluo.WinFormsUI.Docking to more widely used version

- Server
  - Updated for C++ 17 standard, removed majority of deprecation warnings

## 2019-04-24

- Client
  - Added "Browse for Audio" to options menu
  - Added Overlay Mode to context menu
  - Added suppot for level 110

# Historical Changelogs

Server Release Version 2.4.1.0
------------------------------

Dated 08 Feb 2014

1.  Fixed a buffer overflow in extractToBuffer which would cause the client to display spawns on some machines but not others.
2.  Added level 105 support in a previous update but didnt make a note of it, so here it is.
3.  Added concolors by level in a previous update but didnt make a note of it, so here it is.

Server Release Version 2.3.1
---------------------

Dated 14 March 2013

1.  Fixed some of the secondary offset scanning code so it will handle searching for different data type patterns.
2.  Added a debug command, sfu (should have used stfu as it would be funnier), to search for an unsigned int in CharInfo struct, that should point to your character. I added this so I can search for items in primary and secondary, based on their ID. Needs more testing probably.
3.  Added more secondary offset patterns provided by Fireblade. Thanks.
4.  Updated offsets to EQLive dated 3-13-2013
5.  The secondary offset finder will now use the CharInfo offset based on search patterns in config.ini first. If it does not get a match, then it will use what is in the myseqserver.ini file.

Server Release Version 2.3.0
---------------------

Dated 11 February 2013

1.  Added some optimizations to server code. The server code is not very cpu intensive already, but this saved 20-40% when testing.
2.  Fixed a bug where that would cause the client/server to get out of sync.
3.  Changed how server/client interact when switching characters between multiple EQ sessions. This requires both the client and server to be updated to 2.3.0 or higher to work correctly. This fixes an issue that caused the client/server connection to be lost while switching characters.
4.  Improved the detection for a running eqgame.exe process. This should require less stopping and restarting the client connection to maintain a MySEQ session operating.

Server Release Version 2.2.13
----------------------

Dated 24 January 2013

1.  They /system32 folder will be checked for notepad.exe when using the Edit Offsets button. This helps the button work on newer versions of Windows Server. I normally run EQ on Windows Server 2008 R2, so now the button works. Yea!!!
2.  Updated Offsets to EQLive dated 1/16/2013
3.  Added 2 more patterns to secondary offset finder. Thanks Fireblade.

Server Release Version 2.2.12
----------------------

Dated 14 December 2012

1.  Added framework for finding secondary offsets. The patterns and masks are not set up to work yet.
2.  Changed the primary offset finder output text. Maybe it will be less confusing. Maybe not.
3.  Updated offsets to EQLive dated 12/12/2012. Thanks Fireblade.
4.  Reverted the Items offset finder patterns and mask to prior to the last big changes. So it is back to working with what is currently on live.
5.  Moved sending target info after spawns, so on initial startup with a running client that has a target. The target id has something to match. Otherwise targeting only works after retargeting.

Server Release Version 2.2.9
---------------------

Dated 10 November 2012

1.  Updated offset finder patterns. Thanks Fireblade.
2.  Updated offsets to EQLive dated 11/9/2012
3.  Updated some of debug mode code. It still needs more work.

Server Release Version 2.2.8
---------------------

Dated 09 November 2012

1.  Updated server to support ASLR.
2.  Updated offsets to EQLive dated 11/7/2012

Server Release Version 2.2.7
---------------------

Dated 26 October 2012

1.  Moved the 8 bit config settings into the myseqserver.ini file, since it is exe specific.
2.  Updated offsets to EQLive dated 10/12/2012

Server Release Version 2.2.6
---------------------

Dated 27 September 2012

1.  Added the ability for the offset finder to write offsets that have changed to the ini file.
2.  Updated offsets to EQLive dated 9/27/2012

Server Release Version 2.2.5
---------------------

Dated 21 September 2012

1.  Hovering over the minimized system tray icon will now display the server version.
2.  Starting the server with the port unavailable will no longer increment the port number and continue.
3.  If you try to start two sessions of the server on the same listening port, it will now give you an error message when it does not find the port available. That session of the server will exit, rather than start up on the next port number.
4.  Improved the client <-> server connection behavior. If the client and server are connected and eqgame.exe is not running, it will now properly identify the process when it is started.
5.  Added sending over the OwnerID in the spawn structures. This is used to identify pets easier.
6.  Updated offsets to EQLive dated 9/19/2012

Server Release Version 2.2.4
---------------------

Dated 30 August 2012

1.  Single left clicking the minimized system tray icon will no longer restore the server. This is to help prevent accidently restoring the server when running EQ in windowed mode.
2.  Double left clicking the minimized system tray icon will now restore the server to being visible.
3.  Server config parameters will be stored in config.ini. This is separate from the offsets ini file.
4.  Added option to start up the server minimized. This option is accessible on the pull down menu from the minimized system tray icon. It can also by edited directly using the StartMinimized parameter in the config.ini file.
5.  Added a setting in config.ini to use an 8 bit race value in memory. This is enabled by setting EightBitRace=1 in config.ini. Very old clients would use this. Think old trilogy client, for those wanting to know who would have a use for it. This also sets the material parameters to be a 16 bit value in memory.
6.  Fixed some issues with the server when switching between multiple eqgame.exe sessions.

Server Release Version 2.2.3
---------------------

Dated 28 July 2012

1.  Updated myseqserver.ini offsets to EQLive release dated 7-18-2012
2.  Single left clicking the minimized system tray icon will no longer restore the server. This is to help prevent accidently restoring the server when running EQ in windowed mode.
3.  Double left clicking the minimized system tray icon will now restore the server to being visible.
4.  Server config parameters will be stored in config.ini. This is separate from the offsets ini file.
5.  Added option to start up the server minimized. This option is accessible on the pull down menu from the minimized system tray icon. It can also by edited directly using the StartMinimized parameter in the config.ini file.
6.  Moved the Smart Offset Finder search patterns and masks into the config.ini.

Server Release Version 2.2.2
---------------------

Dated 12 July 2012

1.  Integrated Carpathian's Smart Offset Finder into the server. It will only search for primary offsets in the selected eqgame.exe file.

Server Release Version 2.2.1
---------------------

Dated 13 March 2012

1.  Added a displayed IP address. The logic to display this may not always show what you expect for your local lan ip address due to possible virtual interfaces.
2.  Added a button to display all of your ip addresses in a message box.

Server Release Version 2.2.0
---------------------

Dated 05 March 2012

1.  Changed the server from a console application, to a WIN32 API based application.
2.  Added a dialog window to display basic server information.
3.  The executable is now server.exe, instead of myseqserver.exe.
4.  The MySEQ Open Server will now minimize to the System Tray instead of the Taskbar.
5.  The window title caption of the dialog will be the name of the executable.
6.  There are now buttons on the dialog to allow editing, and reloading offsets. The edit function brings up the current ini file in use with notepad.
7.  Running in debug, will bring up a console to interact with.
8.  Code for minimizing to system tray gracefully borrowed from bobobobo's weblog http://bobobobo.wordpress.com which takes code from http://www.gidforums.com/t-5815.html
9.  Changing dialog extended style, so button for dialog shows in taskbar: Code taken from #Winprog FAQ http://winprog.org/faq
10.  I think everything is functional. The old server code will still run fine with this client version. I might have gotten some coding tips from other places. I tried to give credit where I used large hunks of stuff.

Client Release version 2.3.0
---------------------

Dated 11 February 2012

1.  The positions.xml, which stores the docking windows locations and positions, will now be stored in the application data folder.
2.  The prefs.xml and myseq.xml will also be stored in the application data folder now too.
3.  Timers will no longer be loaded or saved for certain zones like guild lobby, nexus, poknowledge.
4.  The character selection in the pull down menu should now show the correct character checked.
5.  Fixed a bug that would cause the character selection menu to not list all the characters available to select.
6.  Fixed a bug that would cause the data stream between the client/server to become corrupted when switching characters.
7.  Removed the refresh list from the character selection menu as it is updated when the menu is pulled down automatically.
8.  Added to the options menu the ability to show the current character name in the application title right after the map name.
9.  Changed the way the server/client interact when running multiple clients and switching between them. This requires using the new server version to work correct.
10.  The level override in the options is no longer limited to having a max value of 85.
11.  The zone name in the window title will now come from what is looked up in the zones.ini file, when the map is loaded.

Client Release version 2.2.13
----------------------

Dated 24 January 2012

1.  Selecting a Ground Item on the item list will now draw a line to the location, even if it is depth filtered.
2.  Using the Mob Search box, will now mark mob respawns on the map too.
3.  The corpse alerts settings will now work with the Mob Search Text box. If you do not want corpses to trigger alerts, then the Mob Search window will behave the same way.
4.  Made a few changes to how the mob name history is saved for spawn timers. This preserves the lists better.
5.  Added some code to help prevent the data stream from becoming corrupted. It has to do with processing incomplete packets of data. Let me know if server/clients lose sync more. I increased the size of the incomplete buffer. And at the end of processing a set of packets, I make sure the incompletebuffer is empty.
6.  Depth Filtering will be saved on a per zone basis. The client will remember the last setting for the depth filter being enabled or not for each zone. These settings are saved in the Application Data folder.
7.  Made some improvements on how the client updates while zoning. This makes the zoning a bit smoother.

Client Release version 2.2.12
----------------------

Dated 14 December 2012

1.  Ground Items list background color now updates with list background color changes.
2.  The target window will now adjust size better so the in game date/time are not cut off.
3.  The target window will adjust height better when selecting spawn timers.
4.  Timers, Spawns, Players, Ground items, should no longer appear on tooltips while hovering on the map if they are depth filtered.
5.  Abbreviated the month in the target window so the minimum width is smaller.
6.  Added a new columns to the spawn timer list which will display a countdown of remaining time in seconds until respawn and the zone shortname.
7.  Selecting a mob on the map, with the left mouse button while pressing the control key, will now work correctly.
8.  Selecting a spawn timer on the map will now update show the selected item on the Spawn Timer List.
9.  Using the Control-Mouse Left button over a mob on the map, will now select the corresponding timer on the Spawn Timer List.
10.  The spawn timer list will change color as the countdown timer gets closer to zero.
11.  Added the ability to Sticky timers. Right clicking a timer on the Spawn Timer List, will have a check box to Sticky the timer. A stickied timer will continue to show even after you zone. No, it won't show the respawn. I am not a miracle worker. But you can see it count down from outside the zone. This is not persistent upon restart. I might add saving these later. But for now, they have to be stickied each time you use them like this.
12.  Changed the timers so that the Last Spawn Name, is now just Spawn Name. It will try to keep the name of possible named mobs listed here. It prefers mobs that start with a capital letter or a #.
13.  Made adjustments in client so that upon startup, it will not clear the target, if there was one already.

Client Release version 2.2.11
----------------------

Dated 22 November 2012

1.  Updated Races.txt.
2.  Changed zoning some in the client to help work better with timers and appearance of spawns when zoning.
3.  Added a cfg/Zones.ini file with the short name and long name of zones. This text is what will now show in the map pane tab when the zone is found.
4.  Significant optimizations were made to the depth filtering code. CPU savings up to 40% are seen in some cases.
5.  Added a + symbol on the map for labels indicating the position where map text location indicates. This is similar to how the in game maps look. That way it is a bit easier to see the location the label specifies.
6.  If multiple map labels are at the same coordinates, subsequent labels will be shifted up when drawn on the map.

Client Release version 2.2.10
----------------------

Dated 16 November 2012

1.  Closed the corner missing dot on map drawn player corpses.
2.  Players drawn on map will now have sharper edges.
3.  Purple border around players indicating they are invis will have better edges.
4.  The Character Selection list will now be dynamically updated when the File menu is opened.
5.  Updated GroundItems.ini. Added a lot of ornamentations and house items. There are still plenty missing, but I filled in quite a few holes.
6.  Fixed serveral exceptions triggered by files not existing, by checking if they exist first. Rather than rely on a try::catch
7.  Fixed out of range exception associated with the con colors array.

Client Release version 2.2.9
---------------------

Dated 14 November 2012

1.  Map will readjust better when changing window size or state.
2.  Saved Settings will be better preserved when updating to a newer version.

Client Release version 2.2.7
---------------------

Dated 26 October 2012

1.  Readded double buffering of the lists. This should make that annoying flickering go away again.
2.  The Mob Search text box now shows a descriptive faded text. More typical behavior of search boxes as we expect in windows applications.
3.  Mob corpses will now look more symmetric.

Client Release version 2.2.6
---------------------

Dated 27 September 2012

1.  Improved handling of manually entering values for zoom and Z depth filtering in tool strip. Moving the mouse off the text box, will no longer steal focus. So it will work like we have traditionally come to expect entering values into text boxes in windows applications.
2.  Improved handling of Lookup text box in tool bar. Deleting the text, will work in place of hitting the reset button.
3.  Reverted pets/familiars con color changes, except on map. On map, they show the con color as gray. Elsewhere, they reflect the real con color. I am not sure which way I like this. Any input is desired on this change.
4.  LDON Objects will now show as con color gray on the map and lists.
5.  Additional CPU optimizations (minor)

Client Release version 2.2.5
---------------------

Dated 21 September 2012

1.  Further reductions in the flicker of the spawn lists. The flicker should be mostly gone now.
2.  Added some optimizations for updating spawns that saves some CPU cycles.
3.  Fixed bugs associated with the Server Address list in options. Only the first two slots were working correctly. Putting server addresses in the additional slots would not function as predicted.
4.  There is now an option in the client to automatically connect to the server upon starting. The default for this is on. You can turn it off by unselecting the "Connect on Startup" in the File menu.
5.  Changed way pets are identified. The owner's spawn id is now sent over from the server. This will require both the server and client to be running at least version 2.2.5.
6.  Player pets, familiars, mounts, will now all show in the spawn list as grey, and on the map as grey. No more running in fear from a player's pet unnecessarily.

Client Release version 2.2.4
---------------------

Dated 30 August 2012

1.  Added new email alerts. Email alerts are only for a zone and not global. Enable the email alerts by pressing the envelope icon on the toolbar. When MySEQ is started, it will always turn off the email alerts. Email alerts will require matching the full text name of the mob, including a # prefix if they have one.
2.  The settings for the SMTP server are in the options dialog on the SMTP tab.
3.  The email uses .net mail. The secure authentication only uses TLS as that is what's in .Net 2.0. This should work for emails like gmail and hotmail.
4.  Password storage and the connections are very basic. There is no certificate verification with the smtp server.
5.  The use network credentials option, will use the credentials used to log on to your computer (or your network) for the smtp login.
6.  If you want to receive the email alert as a text message, there are sms gateways for most wireless networks. A list of some of them are found here: http://en.wikipedia.org/wiki/List\_of\_SMS\_gateways
7.  Updated Races.txt and Classes.txt.
8.  Fixed a bug with matching full text for alerts. It just flat out was not working. It should work now. To match a mob name beginning with #, matching full text, will require having the # in the filters too.
9.  Dynamic Alpha filtering of map lines will is now tied to the Depth Filter. If not enabled, a filtered map line will be completed blank. Enabled a faded line will be drawn.
10.  On the Options->Map tab, there is an option to set the alpha transparency setting of the faded map lines. A setting of 0 is completely transparent, with 100 being the non-transparent setting.

Client Release version 2.2.2
---------------------

Dated 12 July 2012

1.  Fixed typo in one of the Item Alerts.

Client Release version 2.2.1
---------------------

Dated 13 March 2012

1.  Selected targets will stay visible better in the Spawn List window. Moving cursor to the map window will no longer automatically lose focus of a previously focused list, i.e. Spawn List. With auto selection of targets, it will also change the focus to the spawn list when selecting a mob.
2.  The Spawn List, Spawn Timer List and Ground Item List are now double buffered. There should be no more flashing or blinking of the items in the lists.
3.  Because of the way Spawn List grid lines get distorted sometimes scrolling, the new default setting for list grid lines is now off. This is just a more visual tweak, that you only would notice scrolling the lists manually sometimes.
4.  Selecting a value for the map zoom from the pull down menu, will now change the map zoom.
5.  Uninstalling the client will no longer delete the maps folder if other files are present, such as player installed maps.

Client Release version 2.2.0
---------------------

Dated 05 March 2012

1.  The target window will be smaller and more compact by default. If you want the larger detailed version, click the small target window option off under the map pulldown menu settings.
2.  Flashing skittles will have a more constant flash rate with changes in the update delay settings in the options.

Client Release version 2.1.1
---------------------

Dated 05 January 2012

1.  Changed appearance for mobs identified using the lookup option in the toolbar. The is the start of some fancier skittles options. More to come.
2.  Performed some optimizations to reduce CPU utilization by the client.
3.  Client window position will be saved better when closing client.

Client Release version 2.1.0
---------------------

Dated 30 December 2011

1.  Updated Docking Library to DockPanel Suite.
2.  Added option to hide search box in the list panels. This will now be hidden by default.
3.  Combined the toolbars. They are no longer moveable, since this caused issues with the docking panels.

Client Release version 2.0.3
---------------------

Dated 30 November 2011

1.  Updated grounditems.ini from ACTORDEF data from 13th floor.
2.  Updated races.txt from dbstr\_us.txt

Client Release version 2.0.2
---------------------

Dated 25 November 2011

1.  Changed SOE map loading to do all the .txt maps, the same way the client does. It loads the .txt, \_1.txt, \_2.txt and \_3.txt. This makes using the client default map folder work better.
2.  Alert rings are wider now, so they are more visible on the map. They are hard to miss now, and very distinct.
3.  Re-Added the Lookup text box for the map. It is on it's own toolbar now and called "Find Mob" now. Changed the color to blue for the looked up mobs, so it does not match any of the alert filters. For those that do not know what this is, it is like a temporary highlight, mark mobs, option for the map. Try it out. Type the mob name, press enter, tada. It is a wildcard based match, so pretend there is a \* at the beginning and end of the mob name you type.
4.  Typing in mob names in the Find Mob box, will now match using spaces. You no longer have to type the underscore in place of spaces.
5.  The map now supports the 3 sizes of map labels setting for SOE maps. For SEQ maps, the size 2, is the default size. The small size 1 is 90% of the normal size. The large size is 110% normal size.
6.  Added option to add map text label's to the map.
    *   Right clicking a mob, will bring up an option to add text where they mob was located when clicked. Adding text to the SOE map, saves the additional text to the \_2.txt file (SOE maps) or .map file (SEQ maps).
    *   Right clicking a spawn timer, will allow adding text at that spawn location. For locations where multiple mobs have spawned, it will search through the list looking for names that start with # or upper case letters.
    *   Right clicking a mob name in the Spawn List, will work the same as adding map labels by clicking on mobs.
    *   The spawn timer list can also be used to add map labels.
    *   Right clicking a region with no mobs, will have the map label added at the current location of the player.

Client Release version 2.0.1
---------------------

Dated 16 November 2011

1.  Mobs and Ground items can now be added directly to zone alert fiters by right clicking them from the map window. This brings up a context menu, that is different from the normal one in the map window.
2.  Updated the menus, including re-arranging where some items are located.
3.  There is now an option to hide the main menu, depth filtering toolbar, and status bar. Hiding the main menu will also show on the map context menu when it is hidden. It can be made visible again using the context menu of the map window with the menu bar hidden.
4.  Holding the control key down, while right clicking a mob on the map, will show that mob's spawn timer information (if it exists) instead of the mob information.
5.  The options menu will now remember it's previous location, when it is opened again. If the client is closed maximized, it is now possible to restore it to the normal windowed mode.

Client Release version 2.0.0
---------------------

Dated 15 October 2011

1.  Added Ground Items List. Ground Items can now be depth filtered.
2.  Ground Items will also be able to match alerts. The cfg\\GroundItems.ini is where it ties the description to the ActorDef. The alerts match the description text string.
3.  Alert/Filters colors are now clearer on the map. Hunt=Green, Caution=Yellow, Danger=Red, Rare=White.

Server Release Version 2.0.0
---------------------

Dated 15 October 2011

1.  Updated Version to 2.0.0.

Client Release version 1.28.1
----------------------

Dated 12 October 2011

1.  Distinct and faded colors for Drawing pens will now be precalculated when map loads. This saves CPU cycles
2.  All map text distinct colors will also be precalculated.
3.  Added a Distinct Text option for map text under Depth Filter options. This allows more visible text like distinct lines.
4.  Cleaned up some of the depth filtering code to not recalculate pens hardly ever.
5.  Updated races.txt and bodytypes.txt from showeq.

Server Release Version 1.28.1
----------------------

Dated 12 October 2011

1.  Cleaned up way localhost ip address is looked up.

Client Release version 1.28.0
----------------------

Dated 02 October 2011

1.  Updated to compile with VS2010. Updated installer to Wix 3.0.
2.  Fixed some issues with built in con colors for Titanium/SoDalaya client.
3.  There is now a check box in options for alerts, to prevent corpses from triggering alert filters
4.  Alerts on maps will now be depth filtered with the NPCs to which they apply
5.  If an alert type matches a mob, no more alerts will match it. Only one match, then it will stop checking that mob against other alerts.
6.  If Auto Expand is off, the amount of zoom on a map with respect to actual map range will attempted to be maintained when changing zones.
7.  The range circle can now be reduced to a size of 0 to allow getting rid of it altogether on the map.
8.  Map text can now be depth filtered. SEQ format .map files, only have x,y for text so this requires using SOE style maps with x,y,z coordinates for text.

Server Release Version 1.28.0
----------------------

Dated 02 October 2011

1.  The server will now work with versions that have either single and double pointers to ground spawn item offsets.
2.  The myseqserver.exe will now prompt for UAC control to run with appropriate privledges.
3.  The IP addresses being listened to by the server, will list out on starting up the server.

Server Release Version 1.27.3
----------------------

Dated 14 March 2010

1.  Ground Items Offsets updated to a double in-directed pointer.
2.  Updated debugger to find ground items based on a double pointer.

Client Release version 1.27.2
----------------------

Dated 6 November 2009

1.  Fixed bug with spawn timers not saving with SOE or MapFiend maps.

Client Release version 1.27.1
----------------------

Dated 25 October 2009

1.  Fixed bugs with listview sorts on SpawnID and SpawnTime.
2.  Split Corpse Filtering to NPC, PC, and My Corpses. These are on the View menu.
3.  Added option to display a countdown of timers on the Map->Show menu. When this is enabled, timers will show remaining seconds on the map when less than 2 minutes remain.
4.  Moved some of the identification code for pets, familiars, Mercs and Mounts to a location that is called less often in an effort to reduce CPU usage.

Client Release version 1.27.0
----------------------

Dated 18 October 2009

1.  Fixed minor bugs with voice alerts.
2.  Fixed bug with alert prefix and suffix being added to Search.
3.  Hovering mouse on map will now preferentially identify actual NPCs and PCs over pets, mounts and familiars.
4.  Added Primary and Offhand item listing for spawns. The client now requires server 1.27 or higher to work due to change in data stream to support this. The items used for the text translation are from the GroundItems.ini. This is where to add additional items. The GroundItems.ini was created from the weapons header files in SEQ.
5.  Changed drawing order on map, so spawn timers will be drawn on top of corpses.
6.  Moved GroundItems.ini to be loaded into hash table for ground items and primary/offhand lookups. Updated GroundItems.ini file.
7.  Added logging option for spawns under the SpawnList menu. There can be a slight overlap of spawns while zoning due to NPC info for new zone being sent earlier than the zone name in the location in memory where the MySEQ server reads the zone.
8.  This version of client requires 1.27.0 or higher server version. This client is not compatible with older server versions.
9.  Fixed bug with pet regex matches for EQEmu servers."

Server Release Version 1.27
--------------------

Dated 18 October 2009

1.  Added Primary and Offhand offsets. This server requires client version 1.27 or higher.
2.  This server version is not compatible with older clients.

Client Release version 1.26.2
----------------------

Dated 07 September 2009

1.  Added lookup table for Sin and Cos function calls. This resulted in 20-30% reduction in CPU usage in some cases.
2.  Fixed so that on EMU servers for mobs having default directions, the lines will no longer show. Only real direction lines appear now.
3.  Fixed minor bug with mobs staying on alert after proximity alerts turned off.

Client Release version 1.26.1
----------------------

Dated 16 August 2009

1.  Linked Go-Stop button to auto updated when client disconnects from server.
2.  Added Map option to show NPC level only above the skittles.
3.  Added Map option to turn on/off the Auto Expand of Maps when an NPC or PC is outside max map extents.

Client Release version 1.26
--------------------

Dated 01 August 2009

1.  Removed voice selection options due to erroring out with some voices (bugfix).
2.  Added Filter options for depth filtering, so you can select what is filtered
3.  Added PVP display option for players on maps similar to SEQ.
4.  Added PVP level range in options to change settings for range of levels for PVP
5.  Added ability to display player and npc names on maps.
6.  Added built in selectable con colors for EQEmu Server client support.

Server Release Version 1.26
--------------------

Dated 01 August 2009

1.  Added -f \[filename\] option to commandline to allow loading other names of ini files. This option does not allow to specify complete path, only filename with no spaces.

Client Release version 1.25.2
----------------------

Dated 7 May 2009

1.  Fixed bug with MapFiend Maps not loading correctly.

Client Release version 1.25.1
----------------------

Dated 24 April 2009

1.  Minor editorial changes and bug fixes.

Client Release version 1.25
--------------------

Dated 19 April 2009

1.  More tweaks to improve Spawn Timer accuracy and performance.
2.  Selecting a Spawn Timer will now identify the mob that last spawned on the map (if it is still alive). Even if mob is a roamer, it can be identified by selecting the Spawn Timer on list or map.
3.  Consolidated view options to a single list.
4.  Added several optimizations to reduce cpu usage (improvement is very noticable).
5.  Enhanced Depth Filtering (without dynamic alpha) to better view maps. Map lines filtered above will be faded and dashed. Lines filtered below will be faded only. Mobs below will be small dots while mobs above will be hollow circles of normal spawn size. Spawn points above will be normal size but faded. Spawn points below will be small size.

Client Release version 1.24.3
----------------------

Dated 11 April 2009

1.  Changed index for cleared target.

Server Release Version 1.24.1
----------------------

Dated 11 April 2009

1.  Changed Index for Cleared Target.
2.  This Server is compatible with Client 1.24.3 and higher.

Client Release version 1.24.2
----------------------

Dated 11 April 2009

1.  Fixed Spawn Timers - Again

Client Release version 1.24.1
----------------------

Dated 10 April 2009

1.  Fixed bug with Spawn Information Window not going away when set not to show in options.
2.  Fixed a bug with Map Resizing when no maps loaded.
3.  More little tweaks to timers.

Client Release version 1.24
--------------------

Dated 9 April 2009

1.  Made minor changes to improve spawn sizing.
2.  Maps will now automatically resize on loading ensuring all map text is visible.
3.  Added support for displaying EQ game date and time. This requires server 1.24 or higher.
4.  Spawn Information Window will now clear, when you clear your target in game. This requires server 1.24 or higher.
5.  Made several fixes to timers. After establishing timers for respawns, the timers should be maintained even after zoning.

Server Release Version 1.24
--------------------

Dated 9 April 2009

1.  Added offsets for sending EQ date & time to client.  
    New debug command: sfw mm/dd/yyyy  
    The date is the game date from using /time in game. This is used to find world offsets.
2.  Updated behavior of target offsets to clear client target when in game target is cleared.
3.  This Server is compatible with Client 1.19.2 and higher. Only 1.24 client and higher will make use of date & time.

Client Release version 1.23.2
----------------------

Dated 28 February 2009

1.  Fixed various bugs with menus not updating settings and check marks.
2.  Add yellow con range setting to ConLevels.ini. This will be a single setting for level 0. If the yellow range covers three, then the line will read 0=3. If set up for an EQEmu server, where it uses two levels for yellow, it would be 0=2.
3.  Collect Spawn Trails will now always be off on client startup and zoning.

Client Release version 1.23.1
----------------------

Dated 20 February 2009

1.  Fixed some bugs in menus not updating all the time. Added items to menus in more convenient locations. Moved around menus to be more structure oriented.
2.  Playing a wav file for an alerts is now truely asynchronous. No more delays waiting for the sound to finish.
3.  Made improvements to the speech alerts. They now sound more like actual speech (what a concept). They are also asynchronous, in the sense that the map doesn't stop updating when there is speech alerts.
4.  Added back in the cfg/Conlevels.ini. It will use it if the file exists, otherwise the built in cons are used.
5.  Added dialog with filter/alert additions. This allows you to edit any text as it gets added.
6.  Changed Spawn Point colors slightly when counting down to spawn. At 120 Seconds Spawn Point turns yellow. At 90 Seconds Spawn Point turns orange. At 60 Seconds Spawn Point turns Red. At 30 Seconds, Spawn Point starts flashing red.
7.  Support for Shards of Dalaya: Included a SoDConLevels.ini and SoDmyseqserver.ini files, for working with Shards of Dalaya. These do not work with EQLive. Rename to ConLevels.ini, and put in the cfg folder to have proper SoD con colors. Rename the other to myseqserver.ini, and use with the server when connecting to SoD to have the proper offsets.

Client Release version 1.23
--------------------

Dated 31 January 2009

1.  Cleaned up Map grid lines.
2.  Added ability to change color for map grid labels.
3.  Added more Map Optimizations.
4.  Improved Pet Identification.
5.  Changed Spawn Sizing to have less interpolation, making sharper skittles.
6.  Moved Folder Browser to .Net Framework.
7.  Fixed bugs with some alert settings being swapped.
8.  Moved FPS indicator to status bar.
9.  Fixed bug assocated with filters not always clearing before reloading.
10.  Updated Con Colors through level 50.
11.  Fixed a bug when adding Filters/Alerts, if the name contained an apostrophe, it would not match.

Server Release Version 1.23 (January 31, 2009)
---------------------------------------

1.  Fixed Bug where list of Chars was sending each twice
2.  This Server is compatible with Client 1.19.2 and higher

Client Release version 1.19.8
----------------------

Dated 04 January 2009

1.  Performed Optimizations to lines on map loads to speed up map rendering. Minor bug fixes in Depth Filtering.
2.  Moved line filtering for Depth Filters based off variable settings.
3.  Minor tweaks to dynamic alpha line filtering. Fixed inverted z-axis on one of the map line points for SOE maps. It was weird, the point at one end of the line had the z-axis inverted, and the other did not. I found it when I was doing optimizations to connect the dots in map lines.
4.  Moved mapping to use scale and translate transforms. After the transforms and optimizations, map rendering is on the order of 6 times faster. The maps optimized and converted to SEQ .map format using the mapconvert.vbs (included) will provide the best performance. With new optimizations, the converted maps render up to 3 times faster. On my cheap old test computer (2.6 Ghz P4 laptop) the mapfiend Katta Castrum map would render in about 250 milliseconds. This is one of the biggest maps. With converting to SEQ .map format with mapconvert vbs, this map would render in 100 milliseconds (prior to transforms and optimization changes). With the new optimizations the mapfiend map in the native SOE format renders in 40 milliseconds. The optimized SEQ .map rendered in 32 milliseconds. The optimizations take a significant hit when depth filtering is turned on, but performance is still improved. There will be a few second delay before the Katta Castrum mapfiend map displays due to the optimizations. Only for the biggest maps is there any noticeable difference in the map loading times.
5.  Updated all con levels through level 44.
6.  Added Internet Search Option that is clickable from the Spawn List. The default option is to search Allakhazam’s for the mob name. There is a setting for this in the options, and it will use a formatted string. The default is set to "http://everquest.allakhazam.com/search.html?q={0}". The mob name will be entered where the "{0}" is located in the format string. If you change it to search differently, then don’t forget to put in the {0}.

Client Release version 1.19.7
----------------------

Dated 25 December 2008

1.  Cleaned up history of spawn names.  Now it will only keep the actual name, and not the extra 01, 02, 03, extra information.  Only the base name will be used.  Minor cleanup for timers storing spawn information.
2.  Updated all con levels through level 32.
3.  Proximity Alerts!!!!!  The range circle around the player now functions as a proximity alert for mobs.  If they come in the circle, with the alerts turned on it will play a system sound.  The range circle size can be changed by dragging the outer circle.  The proximity alert can be turned on and off by double clicking inside the range circle.  If the hatch inside the circle is visible, then the alerts are turned on.  If the hatch is not visible, then alerts are turned off.  If a mob goes inside the alert circle it will have a flashing circle around it, unless the mob is targeted.  Targeted mobs moving inside the circle, will not set off the alert, as it assumes you are already aware that the mob exist.
4.  Minor cleanup on some colors, where better choices could be made.

Client Release version 1.19.6
----------------------

Dated 21 December 2008

1.  Compiled Docking.bmp as embedded resource, so no longer needs copied to the client folder.
2.  Removed the Vistypes.txt, SpawnTypes.txt and Conlevels.ini, and built them into the client.  Used the pattern of the code from ShowEQ for the Conlevels so it can be updated easily if cons change.  Updated all Con's through level 28.
3.  Invisible mobs will now show with a purple border.  Invisible men types will be a purple skittle, the same as ShowEQ.  Mobs or PCs that are invis type 2 (stealth) will have a flashing purple border.
4.  Spawn points will now appear on zoning, for zones where the timers have gone through a few spawn cycles of mobs to identify spawn locations.
5.  Added ability to clear the timers/spawns for a zone from the menu.
6.  Updated Spawn Timers Window to be selectable.
7.  Updated Spawn Information Window to display selected Spawn Points/Timers.

Client Release version 1.19.5
----------------------

Dated 06 December 2008

1.  Removed Background drawing options.  Windows generally reduces CPU usage of the client enough, that this is no longer needed. Fixed a bug with using map options that was introduced in 1.19.4.
2.  Optimized some checks in drawing alerts to speed up code. Performed some basic code optimizations for speed, reducing Math function calls, etc.
3.  Fixed a bug on first time start up, if you change the address of the ServerIP address used, it will now connect on pressing "Go". Before, you had to restart, or select the server you wanted from the menu again.
4.  Fixed a bug where the spawn timer list window was not clearing all the time. There were more issues, when there was not a map loaded, or if the desired map was not found.
5.  Added selection menu in Options, for selecting different hatch brush styles for the circle pattern around the player.
6.  Added Ability to Change Fonts for Map Text Labels

Client Release version 1.19.4
----------------------

Dated 05 December 2008

1.  Moved mapping to .NET 2.0 managed double buffering. Turned on Anti-Aliasing. Moved spawn sizes to floats, so offsets won't be rounded. This helps clean up skittles on map some more.
2.  Fixed Spawn Timers to save and read properly.
3.  Updated filters to ShowEQ 5.xx compatibility. Added popup to Spawn Lists so you can add/edit/save/reload alerts. If you have old alerts, they will automatically be converted to 5.xx format and then deleted.
4.  Cleaned Up Menus some more.
5.  Some minor updates for voice alerts. There is now an option to select other voices when available.
6.  Fixed crash on startup that was caused by mouse movement trying to look up mobs, before loading has finished. Usually would happened if you connected to server, immediately after startup.
7.  Updated the Color Range Circle around the player to be more consistent with ShowEQ. Updated the con colors through level 10. More have changed at the lower levels probably.
8.  Fixed minor bug when on first time starting up, if you changed folders, the positions.xml and pref.xml would save elsewhere.
9.  Added ability to identify Mercenaries. They will now appear with a border the same color as players, except still being a round NPC spawn style skittle. Enhanced ability to identify player corpses a little better. It should work for all cases now. Added a small ring around selected mobs, the same way normal ShowEQ does.
10.  Added from 1.22, so if you exit while minimized or with the window off screen, the windows will be visible when you restart.
11.  Changed map loading to try and load SEQ .map format files first. If these are optimized, with the "mapconvert.vbs" script included, there is a significant increase in performance due to less lines needed drawn. Just copy this into the folder container your .txt SOE/Mapfiend maps, and run it. It doesn’t do any fancy iteration to make sure it gets rid of extra lines, but will do it sequentially in the file. Then it basically connects the dots, so sequential lines are drawn as longer lines connecting. Added minor changes to map drawing to try and reduce the number of pens destroyed and recreated during map draws.

Client Release version 1.19.3
----------------------

Dated 8 November 2008

1.  Removed MQConsole and Radar.
2.  Updated Con Colors to use the 1.22.x style ConLevels.ini .
3.  Fixed Follow Player and Follow Target map options.
4.  Cleaned Up Menus.
5.  Changed the Force Black Lines options for maps to Force Distinct Lines. It will now always make lines distinct from background.

Server Release Version 1.19.3 (November 8, 2008)
-----------------------------------------

1.  Added Release 1.20.0 content
2.  Added ability to run under Windows Services.  
    \- To install as service run "myseqserver -i"  
    \- To remove windows service run "myseqserver -d"  
    \- To run as windows service copy myseqserver.ini to the System32 folder.  
    \- Start and Stop the service using the Service Control Manager (services.msc).
3.  Fixed some issues with timers.
4.  Changed display message slightly.
5.  This version is compatible with the 1.22 Client.

Server Release Version 1.20.0 (March 22, 2007)
---------------------------------------

1.  Added Beta 1.19.2 content
2.  Added in setproc/getproc packets needed for 1.20 client. Requires 1.20.0 client or greater.
3.  Fixed some bugs where server could potentially hang and consume CPU during zone changes.
4.  Changed display message slightly.
5.  Fixed subtle bug where multiple reconnects could cause spawns and ground items to slowly drop from list.
6.  Added ground item offset scanner to debugger.

Client Release version 1.19.2
----------------------

Dated 27 January 2007

1.  Added ground items back into client. Requires 1.19.2 server or greater.
2.  Added FPS indicator.
3.  Fixed bug with zoom/pan not readjusting screen using keyboard or map controls.

Server Beta Version 1.19.2 (January 27, 2007)
--------------------------------------

1.  Added ground item detection back into server. Requires Beta 1.19.2 client or greater.

Server Release Version 1.19.1 (February 23, 2006)
------------------------------------------

1.  Fixed a bug whereby if the process name was in uppercase, we would not find process.
2.  Added 'scan process' debug menu option.

Server Release Version 1.19.0 (January 22, 2006)
-----------------------------------------

1.  This server has been completely overhauled. It has been re-written almost entirely in C++.
2.  The debug modes were completely overhauled. There is now a command line debug interface for assistance in locating new offsets. There are comments in the INI file to assist you with the new debug interface.
3.  The INI file is completely cleaned out.
4.  INI file changes are reloaded with each connection, so you do not need to restart the server.
5.  There is an auto-close feature that closes the server once EQ shuts down.
6.  These release notes were added back into the server release zipfile.
7.  There are more messages displayed for normal mode operation.

Client Release version 1.19.0
----------------------

Dated 1 January 2006

1.  Fixed bug with saving preferences in wrong folder, making it appear that they were not saved.
2.  Fixed bug with Depth Filter not working when viewing SEQ maps (.map extension). Z-axis values were inverted.
3.  Added 'Force Black Lines' feature under the Map Settings. Enabling this will render all lines in black, ignoring color information in the map file. Greatly speeds up performance.
4.  Added 'Dynamic Alpha' feature under Map Settings. This is a special mode of Depth Filter where map lines are drawn with a varying transparency depending how far either endpoint is from the player (in 3D space). Lines with the closest endpoint <= 75 are drawn fully. Lines with the closest endpoint = 300 are drawn at about 1/8 transparency (ghosted). All other lines are drawn with varying transparency, with the closer lines being more full, and the farther ones being more transparent.
5.  Changed normal Depth Filter mode so that map lines that were previously not drawn, are now drawn, but with 1/8 transparency (ghosted). Map lines are ghosted if both endpoints differ from the player's Z-axis value by more than 75. This is a fixed value. The ZNeg and ZPos fields are only used to filter spawns, not map lines.
6.  Changed map loading order. We will now attempt to load maps like this:
    1.  zonename\_1.txt (Sony format, actual game map)
    2.  zonename.txt (Sony format, mapfiend.net map)
    3.  zonename.map (SEQ format, mapfiend.net map)

    I did this because some of the mapfiend.net maps had incorrect values throughout them, messing up depth filtering. If you want more control over what gets loaded, just remove files from your client/maps folder that are being autoloaded. For example, if you do not want \_1.txt maps being loaded, just remove them from client/maps.

7.  Changed filtering algorithm. The filters have never been shipped officially with version 1.X of MySEQ. Starting with this release, we ship the 6/2005 version 4 list of filters (special thanks to BlueAdept). There are two types of filter files now. The 'base' ones are named 'filters\_zonename.conf' and are shipped with MySEQ. The 'custom' ones are named 'custom\_zonename.conf'. If you have existing filter files, you will need to rename them (to the 'custom' format) for this new version to read them. The global file is now called 'custom\_global.conf'. Do not make changes to the base filters, because they may change with new releases. Keep all your changes in the custom filter files.
8.  There are some new preferences saved, so this will automatically trigger a reset of your preferences file. Sorry.. that's they way it was designed. You will need to re-enter your preferences.
9.  Cleaned up the About box. We now have working hyperlinks to the forums.
10.  Changed some stuff with manual opening of a map file. It would not open the proper Alert files. Now it is a lot smarter.
11.  Improved drawing performance. After adding in the first attempt at the Dynamic Alpha feature, I noticed that the client was using 99% CPU resources to draw a map. The old client used about 55% (on my 2.2GHz test machine). In order to normalize things, I reduced the sampling rate from 250msec to 500msec in the client, and obtained the following CPU utilization information:
12.  Old Client
    Because the old client filtered lines by not drawing them, the CPU usage actually dropped as more of the map was filtered out. The problem was that in most zones, this left too much of the map unrendered, and the depth filter was pretty useless. In the new client, all lines are drawn, but the ones that were previously filtered are ghosted in (drawn at 1/8 transparency). This is why you see the CPU utilization jump from 29% to 39% with Dynamic Alpha off.  
    Also notice that by using the 'Force Black Lines' feature, you can cut the CPU utilization nearly in half. This is a nice option if you are using a slower system that runs the client.
13.  Added 6/2005 4.xx filters to zip release. Thanks BlueAdept!
14.  Added missing file, Docking.bmp, to zip release. This caused unhandled exception popup messages on some users' systems.
15.  Fixed a few typos I came across.

Server Release Version 1.18.2 (December 16, 2005)
------------------------------------------

1.  v1.18.1 required NET v2.0 in order to work.  This new version is identical to v1.18.1 but without the NET v2.0 requirement.

Client Release version 1.18.0
----------------------

    Dated 10 December 2005

1.  It's been awhile since the last update. Minimal changes to the client.
2.  Reversed priority of loading LoY maps over SEQ maps because the SEQ maps have problems and they also have inverted values, which affects the depth filter.
3.  Added new Microsoft Visual Express (2005) C# sln/project files to the source tree.  
    Also fixed a bug with a missing file.
4.  Updated the cfg/Races.txt file from the latest Everquest dbstr\_us.txt file.
5.  Updated the server (separate download) to work with the 12/7/05 patch. Added four debug modes to the server to allow for easier offset hunting.
6.  Changed MyShowEQ in About box to MySEQ Open.
7.  Added simple Makefile to generate binary releases.
8.  The new client was built with Microsoft NET v2.0, so you may need to update to NET v2.0 to use it. Previous releases used NET v1.0 and NET v1.1.  
    NET v2.0 can be downloaded free [here](http://www.microsoft.com/downloads). The server does not use NET, only the client.

Server Release Version 1.18.1 (December 10, 2005)
------------------------------------------

1.  v1.18.0 was accidentally shipped with debug enabled, requiring a special debug DLL to work.  
    This new version is identical to v1.18.0 but without debug enabled.

Server Release Version 1.18.0 (December 9, 2005)
-----------------------------------------

1.  Added new schema flag for walking spawnlist in reverse (required for 12/7/05 patch)
2.  New offsets for 12/7/2005 update
3.  Added GPL messages to source, ini and runtime message
4.  Added new schema flag to allow pSelf to be a direct pointer (required for 11/16/05 patch)
5.  Removed some unused INI entries
6.  Added several new debug modes for help in locating offsets

Client Release version 1.17.0
----------------------

Dated 19 October 2004

1.  Changed the way the client requests packets from the server - you will need version 1.17.0+ of the server. (This will enable us to configure separate update frequencies for each packet type)
2.  Fixed synchronisation of the map context menu.
3.  Made the border colour of Player Characters configurable.
4.  Added 42's Radar changes.
5.  Added 42's "Powesaving mode" - and can now configure options for foreground and background working.

Server Release Version 1.17.0 (October 10, 2004)
-----------------------------------------

1.  Server now corrects problem with reversed X + Y offsets in SpawnInfo  
    \- Existing offset files should work without change.  
    \- New offset files will have a SchemaVersion=2 line to indicate that the X + Y offsets are NOT reversed...
2.  Made a Major change to how the Client requests data from the server. This will make the process much more flexible.  
    However it does mean that the new server will ONLY work with client 1.17.0+
3.  Changed the version number to match the client to help indicate which client + server can talk to each other.  
    Note the build number does not need to match only the Major + minor versions i.e. 1.17.\*

Client Release version 1.16.3
----------------------

Dated 30 September 2004

1.  Fixed up spawnInfo so that X contains the X co-ordinate for Player + Spawn packets. (They are still reversed in the packet stream - I will probably release a new server to correct this soon)
2.  Changed the way "Center on Player" works to restore the original behavior but add in the ability to keep the player or target centered  
    "Follow None" = Do not scroll the map  
    "Follow Player" = move map to keep player in view  
    "Follow target" = move map to keep target in viewtered on screen  
    Keep Centered" = The follow target (either player or target) will remain in the same position on the screen, even if this causes most of the map to be off the screen.
3.  Integrated 42!'s latest changes for doing a quick lookup for spawns by name.

Client Release version 1.16.2
----------------------

Dated 28 September 2004

1.  Added all missing Con Level Information - please post feedback if any of the con level data is incorrect.
2.  Fixed the MobInfo panel background color. (Thanks CybMax for spotting it).
3.  Added the ability to select which character to monitor when multiboxing.  
    Note: The list does not refresh automatically - you must click on the refresh menu item. (this is an attempt to keep the overhead of this feature to a minimum).  
    Also Note that a new server version is required for this to work...
4.  Added 42!'s ability to select a point on the map for navigation purposes. Shift Left-click on the map to toggle the selected point.  
    (I changed 42's code a little to preserve the default behavior for selecting spawns, also made the line dashed so that it is easy to distinguish from the selected spawn)

Server Release Version 1.12.0 (September 28, 2004)
-------------------------------------------

1.  Added the ability to select which EQ Process to monitor when multi-boxing.

Note: MySeq Client V1.16.2 or better is required to perform this action.

Client Release version 1.16.1
----------------------

Dated 18 September 2004

1.   Updated program to read con level data from an ini file.
2.   Updated Con Level Information to include level 66-70 (using extrapolated data)
3.   Added new race names for OOW

Server Release Version 1.11.0 (November 26, 2003)
------------------------------------------

1.  Changed the version number becuase there had been multiple versions of Server 1.9b which made it difficult to troubleshoot issues.

Server Release Version 1.9b.0 (October 13, 2003)
-----------------------------------------

1.  Fixed problem with player level when riding a horse - (Added another level of indirection. CharInfoPtr->CharInfo Struct.  CharInfo.SpawnInfoPtr->SpawnInfo Struct.)
