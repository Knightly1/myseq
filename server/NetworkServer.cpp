/*==============================================================================
	Copyright (C) 2006 - 2013  All developers at http://sourceforge.net/projects/seq

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  ==============================================================================*/

#include "StdAfx.h"
#include "NetworkServer.h"
#include <stdlib.h>
#include <IPHlpApi.h>
#include <ws2tcpip.h>

NetworkServer::NetworkServer()
{
	sockAddrSize = sizeof(sockAddr);
	psockAddrIn = (sockaddr_in*) &sockAddr;
	zoneName = "StartUp";
	sockClient = INVALID_SOCKET;
	LastProcess = 0;
	change_process = false;
}

NetworkServer::~NetworkServer(void)
{
	WSACleanup();
}

std::vector<std::string> NetworkServer::listIPAddresses(bool showMessage /* = true */, bool firstOnly /* = false */)
{
	// Allocate 15kb + the size, just to be safe
	ULONG buffer_length = sizeof(IP_ADAPTER_INFO) + 15000;

	std::vector<std::string> returnVector;
	std::string output = "No IPv4 Addresses Found";
	auto pAdapterInfo = static_cast<IP_ADAPTER_INFO*>(HeapAlloc(GetProcessHeap(), 0, buffer_length));
	if (pAdapterInfo == nullptr)
	{
		output = "Failed to allocate memory.";
	}
	else
    {
	    int retVal = GetAdaptersInfo(pAdapterInfo, &buffer_length);
		if (retVal == ERROR_BUFFER_OVERFLOW)
		{
		    output = "Error allocating memory.";
		}
		else if (retVal == NO_ERROR)
		{
			output = "";
            PIP_ADAPTER_INFO pAdapter = pAdapterInfo;
			bool breakEarly = false;
			while (pAdapter && !breakEarly)
			{
				if (strcmp(pAdapter->IpAddressList.IpAddress.String, "0.0.0.0") != 0)
				{
					returnVector.push_back(pAdapter->IpAddressList.IpAddress.String);
                    output += pAdapter->IpAddressList.IpAddress.String;
    		        output += "\r\n";
					breakEarly = firstOnly;
				}
				pAdapter = pAdapter->Next;
			}
		}
		HeapFree(GetProcessHeap(), 0, pAdapterInfo);
    }

	// Add the local loopback if we don't already have it, but only if there were no errors and it doesn't already exist
	if ((returnVector.empty() && output.empty()) || (!firstOnly && std::count(returnVector.begin(), returnVector.end(), "127.0.0.1") < 1))
	{
		returnVector.push_back("127.0.0.1");
		output += "127.0.0.1\r\n";
	}

	if (showMessage)
	{
        MessageBox(h_MySEQServer ? h_MySEQServer: nullptr, output.c_str(), "MySEQ Open Server: Local IP Addresses", MB_OK | MB_TOPMOST | MB_ICONINFORMATION);
	}

	return returnVector;
}

bool NetworkServer::openListenerSocket(bool service)
{
	WSADATA wsa;

	if (WSAStartup(MAKEWORD(1,1), &wsa) != 0)
	{
		MessageBox(NULL, "Error: NetworkServer: Failed to initialize Winsock.", "Failed to initialize Winsock", 0);
		// ostrstream strm;
		return false;
		// strm << "Error: NetworkServer: Failed to initialize Winsock." << ends ; \
		// throw Exception(EXCLEV_ERROR, strm.str());
	}
	// Attempt to get a socket
	sockListener = socket(AF_INET, SOCK_STREAM, 0);
	if (sockListener == INVALID_SOCKET)
	{
		MessageBox(NULL, "Error: NetworkServer: Error creating listener socket.", "Failed to create listener socket.", 0);
		return false;
		//ostrstream strm;
		//strm << "Error: NetworkServer: Error creating listener socket." << ends ; \
		//throw Exception(EXCLEV_ERROR, strm.str());
	}

	// Fill out the sockaddr structure with typical values
	memset(&sockAddr, 0, sockAddrSize);
    psockAddrIn->sin_family = AF_INET;

	psockAddrIn->sin_addr.s_addr = INADDR_ANY;

	// Attempt to bind the listener socket
	psockAddrIn->sin_port = htons(port);

	if ( bind(sockListener, (struct sockaddr*)&sockAddr, sockAddrSize) == SOCKET_ERROR )
	{
		std::cout << "MySEQServer: Failed binding to port: " << port << std::endl;
		std::string str("Failed binding to port: ");
		std::stringstream strm;
		strm << std::dec << port;
		str.append(strm.str());
		str.append("\r\nCheck for a suitable port or currently running MySEQ Server.\r\nExiting.");
		MessageBox(NULL, str.c_str(), "WindSock Error: Failed binding to port.", MB_OK | MB_TOPMOST | MB_ICONERROR);
		return false;
	}

	// Setup the backlog
	if (service) {
		if (listen(sockListener, 10) == SOCKET_ERROR)
		{
			MessageBox(NULL, "Error: NetworkServer: Listen request failed.", "Listen request failed", 0);
			return false;
			//ostrstream strm;
			//strm << "Error: NetworkServer: Listen request failed with code " << dec << WSAGetLastError() << ends ;
			//throw Exception(EXCLEV_ERROR, strm.str());
		}
	} else {
		// Setup the backlog
		if (listen(sockListener, 10) == SOCKET_ERROR)
		{
			MessageBox(NULL, "Error: NetworkServer: Listen request failed.", "Listen request failed", 0);
			return false;
			//ostrstream strm;
			//strm << "Error: NetworkServer: Listen request failed with code " << dec << WSAGetLastError() << ends ;
			//throw Exception(EXCLEV_ERROR, strm.str());
		}
		//Switch to Non-Blocking mode
		WSAAsyncSelect(sockListener, hwnd, 1045, FD_READ | FD_CONNECT | FD_CLOSE | FD_ACCEPT);

		auto listIPAddresses = NetworkServer::listIPAddresses(false);
		for (auto& ipAddress : listIPAddresses)
		{
    	    std::cout << "MySEQServer: Listening on " << ipAddress << ":" << std::dec << port << std::endl;
		}

		// Set the port in the gui
		if (h_MySEQServer != nullptr) {
			SetDlgItemText(h_MySEQServer, IDC_TEXT_PORT, std::to_string(port).c_str());
			// Set the best ip address in the gui
			SetDlgItemText(h_MySEQServer, IDC_TEXT_PRIMARY, listIPAddresses.empty() ? "UNKNOWN" : listIPAddresses[0].c_str());
		}
	}
	return true;
}

void NetworkServer::openClientSocket()
{
	// Wait for incoming connections
	sockClient = accept(sockListener, &sockAddr, &sockAddrSize);

	if (sockClient == INVALID_SOCKET)
	{
		MessageBox(NULL, "Error: NetworkServer: Error with connection request.", "Error with connection request", 0);
		/*
	    std::ostrstream strm;
		strm << "Error: NetworkServer: Error with connection request 0x" << std::dec << WSAGetLastError() << std::ends ;
		throw Exception(EXCLEV_ERROR, strm.str());
		*/
		exit(EXCLEV_ERROR);
	}
	zoneName = "StartUp";
	if (h_MySEQServer) {
		SetDlgItemText(h_MySEQServer, IDC_TEXT_ZONE, _T(""));
		SetDlgItemText(h_MySEQServer, IDC_TEXT_NAME, _T(""));
	}

	char incomingIP[INET_ADDRSTRLEN] = { 0 };
	inet_ntop(AF_INET, &psockAddrIn->sin_addr, incomingIP, INET_ADDRSTRLEN);
    std::cout << "MySEQServer: New connection from: " << incomingIP << std::endl;
}

void NetworkServer::closeClientSocket()
{
	if (sockClient != INVALID_SOCKET) {
        std::cout << "MySEQServer: Closing client socket" << std::endl << std::endl;
		closesocket(sockClient);
		sockClient = INVALID_SOCKET;
	}
}

void NetworkServer::closeListenerSocket()
{
    std::cout << "MySEQServer: Closing listener socket" << std::endl << std::endl;
	closesocket(sockListener);
	sockListener = INVALID_SOCKET;
}
bool NetworkServer::requestContains(inc_packet_types pt)
{
	return ((clientRequest & pt) != 0);
}

std::string NetworkServer::getCharName(MemReaderInterface* mr_intf)
{
	// get current character name
	std::string rtn = "";
	UINT pTemp = 0;
	UINT nameOff;

	if (offsets[OT_self])
		pTemp = mr_intf->extractRAWPointer(offsets[OT_self]);

    nameOff = spawnParser.offsets[spawnParser.OT_name];
	if (pTemp)
	{
		rtn = mr_intf->extractString(pTemp + nameOff);
	}
	return rtn;
}

void NetworkServer::setOffset(offset_types ot, UINT value)
{
	offsets[ot] = value;
}

void NetworkServer::init(IniReaderInterface* ir_intf)
{
	spawnParser.init(ir_intf);
	itemParser.init(ir_intf);
	worldParser.init(ir_intf);

	port = ir_intf->readIntegerEntry("Ports", "ListenPort", true);

	setOffset(OT_spawnlist,	ir_intf->readIntegerEntry("Memory Offsets", "SpawnHeaderAddr"));
	setOffset(OT_self,		ir_intf->readIntegerEntry("Memory Offsets", "CharInfo"));
	setOffset(OT_target,	ir_intf->readIntegerEntry("Memory Offsets", "TargetAddr"));
	setOffset(OT_zonename,	ir_intf->readIntegerEntry("Memory Offsets", "ZoneAddr"));
	setOffset(OT_ground,	ir_intf->readIntegerEntry("Memory Offsets", "ItemsAddr"));
	setOffset(OT_world,		ir_intf->readIntegerEntry("Memory Offsets", "WorldAddr"));

	if (h_MySEQServer) {
		// Set offsets in GUI
		TCHAR ot_spawnlist[16];
		TCHAR ot_self[16];
		TCHAR ot_target[16];
		TCHAR ot_zonename[16];
		TCHAR ot_ground[16];
		TCHAR ot_world[16];
		// Put in our offsets in a hex format
		sprintf_s(ot_spawnlist,"0x%x",offsets[OT_spawnlist]);
		sprintf_s(ot_self,"0x%x",offsets[OT_self]);
		sprintf_s(ot_target,"0x%x",offsets[OT_target]);
		sprintf_s(ot_zonename,"0x%x",offsets[OT_zonename]);
		sprintf_s(ot_ground,"0x%x",offsets[OT_ground]);
		sprintf_s(ot_world,"0x%x",offsets[OT_world]);
		// Update the dialog
		SetDlgItemText(h_MySEQServer, IDC_TEXT_SPAWNHEADER, ot_spawnlist);
		SetDlgItemText(h_MySEQServer, IDC_TEXT_CHARINFO, ot_self);
		SetDlgItemText(h_MySEQServer, IDC_TEXT_TARGETADDR, ot_target);
		SetDlgItemText(h_MySEQServer, IDC_TEXT_ZONEADDR, ot_zonename);
		SetDlgItemText(h_MySEQServer, IDC_ITEMSADDR, ot_ground);
		SetDlgItemText(h_MySEQServer, IDC_TEXT_WORLDADDR, ot_world);
	}

    std::cout << "MySEQServer: Memory offsets read in." << std::endl;
}

void NetworkServer::enterReceiveLoop(MemReaderInterface* mr_intf)
{
	bool exitLoop = false;

	while (!exitLoop)
	{
		exitLoop = processReceivedData(mr_intf);
	}
}

UINT NetworkServer::current_offset(int type)
{
		return (UINT)offsets[(offset_types)type];
}

bool NetworkServer::processReceivedData(MemReaderInterface* mr_intf)
{
	int bytesRecvd, maxLoop;
	UINT pTemp, pTemp2;
	std::string newZoneName;
	int numSpawns = 0, numItems = 0, numElements;

	quickInfo = false;

	if (LastProcess != mr_intf->getCurrentPID()) {
		quickInfo = true;
		LastProcess = mr_intf->getCurrentPID();
	}

	// Wait for request from the client
	bytesRecvd = recv(sockClient, (char*)&clientRequest, sizeof(clientRequest), 0);

	if (bytesRecvd == 0 || bytesRecvd == SOCKET_ERROR || bytesRecvd != sizeof(clientRequest))
	{
		return true;
	}

	if (change_process) // The last request was to change the process.  So this packet
						// should contain the process id, to switch to
	{
		DWORD originalPID = mr_intf->getCurrentPID();
        std::cout << "MySEQServer: Setting process to 0x" << std::hex <<  clientRequest << std::endl;
		mr_intf->openFirstProcess("eqgame", false);
		while (1)
		{
			if (mr_intf->getCurrentPID() == (DWORD)clientRequest) {
				zoneName = "StartUp";
				SetDlgItemText(h_MySEQServer, IDC_TEXT_ZONE, _T(""));
				SetDlgItemText(h_MySEQServer, IDC_TEXT_NAME, _T(""));
				break;
			}
			if (!mr_intf->openNextProcess("eqgame"), false)
				break;
		}
		if (mr_intf->getCurrentPID() != (DWORD)clientRequest)
		{
			// we failed to connect to the desired PID
			// lets see if we can open a process
			if (mr_intf->openFirstProcess("eqgame", false))
			{
				if (originalPID != mr_intf->getCurrentPID())
				{
					zoneName = "StartUp";
					SetDlgItemText(h_MySEQServer, IDC_TEXT_ZONE, _T(""));
					SetDlgItemText(h_MySEQServer, IDC_TEXT_NAME, _T(""));
				}
			}
		}
		// the client no longer expects a return packet for this.
		// the client continue requesting data the next tick().
		//spawnParser.packNetBufferRaw(OPT_process, mr_intf->getCurrentPID());
		//spawnParser.pushNetBuffer();
		change_process = false;
		return false;
	}

	if ( !mr_intf->isValid() ) {
		// we dont have a valid process
		// send a reply to keep stream connected

		spawnParser.packNetBufferRaw(OPT_process, 0);
		spawnParser.pushNetBuffer();
		// First send the number of elements
		numElements = spawnParser.getNetBufferSize();
		send(sockClient, (char*) &numElements, sizeof(numElements), 0);

		// Now sent the array of elements
		if ( numElements )
		{
			// Show the spawncount (minus the zonename and yourself, if there is targeted mob we will get one extra)
			if (quickInfo)
                std::cout << "MySEQServer: numSpawns(" << std::dec << numSpawns << ") numItems(" << numItems << ")" << std::endl;

			send(sockClient, (char*) spawnParser.getNetBufferStart(), numElements * sizeof(netBuffer_t), 0);
			spawnParser.clearNetBuffer();
			numSpawns = numItems = 0;
		}

		return false;
		//return true;  // return true, if not good receive
	}

	// Send get_process requests
	if ( requestContains(IPT_getproc) )
	{
		pTemp = 0;
		MemReader tempMemReader;

		// Look for the first available process match
		tempMemReader.openFirstProcess("eqgame");
		while (tempMemReader.isValid())
		{
			if (offsets[OT_self])
				pTemp = tempMemReader.extractRAWPointer(offsets[OT_self]);

			if (pTemp)
			{
				if (tempMemReader.extractToBuffer(pTemp, spawnParser.rawBuffer, spawnParser.largestOffset))
				{
					// For this type of packet, we only use the gamer name and the PID
					spawnParser.packNetBufferRaw(OPT_process, tempMemReader.getCurrentPID());
					spawnParser.pushNetBuffer();
				}
			}

			// Look for the next available process match
			if (!tempMemReader.openNextProcess("eqgame"))
				break;
		}
	}

	// Send set_process requests
	if ( requestContains(IPT_setproc) )
	{
		change_process = true;
		return false;
		// client does not expect a return packet now

	}

	// Send zonename requests
	if ( requestContains(IPT_zone) )
	{
		if (offsets[OT_zonename])
			newZoneName = mr_intf->extractString2(offsets[OT_zonename] - 0x400000 + (UINT)mr_intf->getCurrentBaseAddress());

		// Only send zonename response if zone changed
		if ( newZoneName != zoneName )
		{
			quickInfo = true;
			zoneName = newZoneName;
			if (h_MySEQServer) {
				if (zoneName != "StartUp") {
					std::string charName = getCharName(mr_intf);
					SetDlgItemText(h_MySEQServer, IDC_TEXT_NAME, charName.c_str());
					SetDlgItemText(h_MySEQServer, IDC_TEXT_ZONE, zoneName.c_str());
					// force an update of the spawns count numbers on gui
					loopCount = 50;
				} else {
					SetDlgItemText(h_MySEQServer, IDC_TEXT_NAME, _T(""));
					SetDlgItemText(h_MySEQServer, IDC_TEXT_ZONE, _T(""));
				}
			}
			spawnParser.packNetBufferStrings(OPT_zone, zoneName, "");
			spawnParser.pushNetBuffer();
		}

		if (quickInfo)
            std::cout << "MySEQServer: Zonename is " << newZoneName << std::endl;
	}

	// Send player requests
	if ( requestContains(IPT_self) )
	{
		pTemp = 0;

		if (offsets[OT_self])
			pTemp = mr_intf->extractRAWPointer(offsets[OT_self]);

		if (quickInfo)
            std::cout << "MySEQServer: pSelf is 0x" << std::hex << pTemp << std::endl;

		if (pTemp)
			if (mr_intf->extractToBuffer(pTemp, spawnParser.rawBuffer, spawnParser.largestOffset))
			{
				spawnParser.packNetBufferRaw(OPT_self, pTemp);
				spawnParser.pushNetBuffer();
			}
	}

	// Send spawnlist requests
	if ( requestContains(IPT_spawns) )
	{
		pTemp = pTemp2 = 0;

		if (offsets[OT_spawnlist])
			pTemp = pTemp2 = mr_intf->extractRAWPointer(offsets[OT_spawnlist]);

		/* As of TSS, after shrouds or hover, this pointer may point to a spawn in the
			middle of the list. Back up to the top of the spawn list just to be sure we
			grab the whole thing. */
		for (maxLoop=0; maxLoop<2000; maxLoop++)
		{
			if (mr_intf->extractToBuffer(pTemp, spawnParser.rawBuffer, spawnParser.largestOffset))
			{
				if (spawnParser.extractPrevPointer())
					pTemp = spawnParser.extractPrevPointer();
				else
					break;
			}
		}

		if (quickInfo)
		{
            std::cout << "MySEQServer: pSpawnlist is 0x" << std::hex << pTemp << std::endl;

			if (maxLoop == 2000)
                std::cout << "MySEQServer: Warning: maxLoop reached finding pSpawnlist!" << std::endl;

			if (pTemp != pTemp2)
                std::cout << "MySEQServer: pSpawnlist changed from INI setting of 0x" << std::hex << pTemp2 << std::endl;
		}
		loopCount++;

		while (pTemp)
		{
			if (mr_intf->extractToBuffer(pTemp, spawnParser.rawBuffer, spawnParser.largestOffset))
			{
				spawnParser.packNetBufferRaw(OPT_spawns, pTemp);
				spawnParser.pushNetBuffer();
				numSpawns++;
				pTemp = spawnParser.extractNextPointer();
			}
			else
				pTemp = 0;
		}
		if (loopCount > 20) {
			// Update the gui with the spawn/item counts
			if (offsets[OT_spawnlist])
			pTemp = mr_intf->extractRAWPointer(offsets[OT_spawnlist]);

			// Get us to the top of the list
			for (maxLoop=0; maxLoop<2000; maxLoop++)
			{
				if (mr_intf->extractToBuffer(pTemp, spawnParser.rawBuffer, spawnParser.largestOffset))
				{
					if (spawnParser.extractPrevPointer())
						pTemp = spawnParser.extractPrevPointer();
					else
						break;
				}
			}
			UINT typeOffset = spawnParser.offsets[spawnParser.OT_type];
			BYTE result;
			int pcNum=0, npcNum=0, corpseNum=0;
			while (pTemp)
			{
				if (mr_intf->extractToBuffer(pTemp, spawnParser.rawBuffer, spawnParser.largestOffset))
				{
					result = spawnParser.extractRawByte(spawnParser.OT_type);
					switch(result)
					{
						case 0:
							pcNum++;
							break;
						case 1:
							npcNum++;
							break;
						default:
							corpseNum++;
					}
					pTemp = spawnParser.extractNextPointer();
				}
				else
					pTemp = 0;
			}

			char buffer[65];
			_itoa_s( npcNum, buffer, 65, 10);
			SetDlgItemText(h_MySEQServer, IDC_TEXT_SPAWNS, (LPCSTR) &buffer);
			_itoa_s( pcNum, buffer, 65, 10);
			SetDlgItemText(h_MySEQServer, IDC_TEXT_SPAWNS2, (LPCSTR) &buffer);
			_itoa_s( corpseNum, buffer, 65, 10);
			SetDlgItemText(h_MySEQServer, IDC_TEXT_SPAWNS3, (LPCSTR) &buffer);
		}
	}

		// Send target requests
	if ( requestContains(IPT_target) )
	{
		pTemp = 0;

		if (offsets[OT_target])
			pTemp = mr_intf->extractRAWPointer(offsets[OT_target]);

		if (quickInfo)
            std::cout << "MySEQServer: pTarget is 0x" << std::hex << pTemp << std::endl;

		if (pTemp)
		{
			if (mr_intf->extractToBuffer(pTemp, spawnParser.rawBuffer, spawnParser.largestOffset))
			{
				spawnParser.packNetBufferRaw(OPT_target, pTemp);
				spawnParser.pushNetBuffer();
			} else {
				// Send target of spawn ID 99999 (no target)
				spawnParser.packNetBufferEmpty(OPT_target, pTemp);
				spawnParser.pushNetBuffer();
			}
		} else {
				// Send target of spawn ID 99999 (no target)
				spawnParser.packNetBufferEmpty(OPT_target, pTemp);
				spawnParser.pushNetBuffer();
			}
	}

	// Send grounditem requests
	if ( requestContains(IPT_ground) )
	{
		pTemp = pTemp2 = 0;
		UINT nameOff;
		std::string itName = "";
		nameOff = itemParser.offsets[itemParser.OT_name];

		if (offsets[OT_ground])
			pTemp2 = mr_intf->extractRAWPointer(offsets[OT_ground]);

		if (pTemp2)
			itName = mr_intf->extractString(pTemp2 + nameOff);

		if (itName.compare(0,2,"IT") == 0) {
			pTemp = pTemp2;
		} else if (pTemp2) {
			pTemp = mr_intf->extractPointer(pTemp2);
		}
		if (quickInfo)
		{
            std::cout << "MySEQServer: pItems is 0x" << std::hex << pTemp << std::endl;
		}

		while (pTemp)
		{
			if (mr_intf->extractToBuffer(pTemp, itemParser.rawBuffer, itemParser.largestOffset))
			{
				itemParser.packItemBuffer(OPT_ground);
				spawnParser.packNetBufferFrom(itemParser);
				spawnParser.pushNetBuffer();
				numItems++;
				// Avoid infinite loops
				if ((numItems > 300) || (pTemp == itemParser.extractNextPointer()) )
					pTemp = 0;
				else
					pTemp = itemParser.extractNextPointer();
			}
			else
				pTemp = 0;
		}
		if (loopCount > 20) {
			// Update the gui with the spawn/item counts
			char buffer[65];
			_itoa_s( numItems, buffer, 65, 10);
			SetDlgItemText(h_MySEQServer, IDC_TEXT_ITEMS, (LPCSTR) &buffer);

			loopCount = 0;
		}
	}

	// Send world requests
	if ( requestContains(IPT_world) )
	{
		pTemp = 0;

		if (offsets[OT_world])
			pTemp = mr_intf->extractRAWPointer(offsets[OT_world]);

		if (quickInfo)
            std::cout << "MySEQServer: pWorldInfo is 0x" << std::hex << pTemp << std::endl;

		if (pTemp)
			pTemp = pTemp;

		if (pTemp)
			if (mr_intf->extractToBuffer(pTemp, worldParser.rawBuffer, worldParser.largestOffset))
			{
				worldParser.packWorldBuffer(OPT_world);
				spawnParser.packNetBufferWorld(worldParser);
				spawnParser.pushNetBuffer();
			}
	}

	// Send spawn information (most other information is also packed into a spawn structure)
	numElements = spawnParser.getNetBufferSize();

	// First send the number of elements
	send(sockClient, (char*) &numElements, sizeof(numElements), 0);

	// Now sent the array of elements
	if ( numElements )
	{
		// Show the spawncount (minus the zonename and yourself, if there is targeted mob we will get one extra)
		if (quickInfo)
            std::cout << "MySEQServer: numSpawns(" << std::dec << numSpawns << ") numItems(" << numItems << ")" << std::endl;

		send(sockClient, (char*) spawnParser.getNetBufferStart(), numElements * sizeof(netBuffer_t), 0);
		spawnParser.clearNetBuffer();
		numSpawns = numItems = 0;
	}
	return false;
}