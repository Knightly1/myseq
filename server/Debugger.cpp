/*==============================================================================

	Copyright (C) 2006-2013  All developers at http://sourceforge.net/projects/seq

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  ==============================================================================*/

#include "StdAfx.h"

#include "Debugger.h"

#include <cstdlib>

#define TO_LOWER(str) (transform(str.begin(), str.end(), str.begin(), (int(*)(int))tolower))

#define DISPLAY_SPAWN_ITEM(off, member) std::cout << "    " << spawnParser.ptrNames[spawnParser.off] << " -> " << spawnParser.tempNetBuffer.member << std::endl

#define DISPLAY_SPAWN_ITEMI(off, member) std::cout << "    " << spawnParser.ptrNames[spawnParser.off] << " -> " << (UINT) spawnParser.tempNetBuffer.member << std::endl



Debugger::Debugger(){}



bool Debugger::setOffset(offset_types ot, UINT value)

{

	if (ot >= OT_max)

		return false;



	offsets[ot] = value;

	return true;

}



void Debugger::setOffset(offset_types ot, UINT value, std::string ptrName)

{

	setOffset(ot, value);

	ptrNames[ot] = ptrName;

}



void Debugger::init(IniReaderInterface* ir_intf)

{

	spawnParser.init(ir_intf);

	itemParser.init(ir_intf);

	worldParser.init(ir_intf);



	setOffset(OT_spawnlist,	ir_intf->readIntegerEntry("Memory Offsets", "SpawnHeaderAddr"), "pSpawns");

	setOffset(OT_self,		ir_intf->readIntegerEntry("Memory Offsets", "CharInfo"), "pSelf");

	setOffset(OT_target,	ir_intf->readIntegerEntry("Memory Offsets", "TargetAddr"), "pTarget");

	setOffset(OT_zonename,	ir_intf->readIntegerEntry("Memory Offsets", "ZoneAddr"), "pZone");

	setOffset(OT_ground,	ir_intf->readIntegerEntry("Memory Offsets", "ItemsAddr"), "pItems");

	setOffset(OT_world,		ir_intf->readIntegerEntry("Memory Offsets", "WorldAddr"), "pWorld");

    std::cout << "Debugger: Memory offsets read in." << std::endl;

}



void Debugger::printMenu()

{
    std::cout << std::endl;

    std::cout << "   (d)isplay / (r)eload offsets" << std::endl;

//	cout << "    r) reload all offsets from INI file" << endl;

    std::cout << "  spo) set a primary offset   (index/name) (hex value)" << std::endl;

    std::cout << "  sso) set a secondary offset (index/name) (hex value)" << std::endl;

    std::cout << "   ez) examine data using pZone (et) pTarget (ew) pWorld" << std::endl;

    std::cout << "   es) examine raw data using pSelf" << std::endl;

//	cout << "   et) examine raw data using pTarget" << endl;

//	cout << "   ew) examine raw data using pWorld" << endl;

    std::cout << "   fz) find zonename using pZone (zonename)" << std::endl;

    std::cout << "   ft) find spawnname using pTarget (fs) pSelf (spawnname)" << std::endl;

//	cout << "   fs) find spawnname using pSelf (spawnname)" << endl;

    std::cout << "   ps) display spawn info using pSelf (pt) pTarget" << std::endl;

//	cout << "   pt) display spawn info using pTarget" << endl;

    std::cout << "   sp) scan process names (process name)" << std::endl;

    std::cout << "  sft) scan for float using pTarget (sfs) pSelf (X,Y,Z)" << std::endl;
//	cout << "  sfs) scan for floating point using pSelf (X,Y,Z)" << endl;

    std::cout << "  sfa) scan for floating point using Address (X,Y,Z,Address)" << std::endl;

    std::cout << "  sfu) scan for UINT using pSelf (int)" << std::endl;

    std::cout << "  sfw) scan for world using Game Date (mm/dd/yyyy) from /time" << std::endl;

    std::cout << "   sg) scan for ground items" << std::endl;

    std::cout << "   ws) walk the spawnlist (reverse) using pSelf (wt) pTarget" << std::endl;

//	cout << "   wt) walk the spawnlist (reverse) using pTarget" << endl;

    std::cout << "   vs) walk the spawnlist (forward) using pSelf (vt) pTarget" << std::endl;

//	cout << "   vt) walk the spawnlist (forward) using pTarget" << endl;

    std::cout << "    x) exit debugger" << std::endl;

    std::cout << std::endl;

}

void Debugger::enterDebugLoop(MemReaderInterface* mr_intf, IniReaderInterface* ir_intf)

{

	std::string userInput;



	init(ir_intf);

	printMenu();

	while (1)

	{
        std::cout << " > ";

		getline(std::cin, userInput);

		if (userInput.compare(0, 1, "?") == 0)

			printMenu();

		else if (userInput.compare(0, 1, "d") == 0)

			displayCurrentOffsets();

		else if (userInput.compare(0, 1, "r") == 0)

			init(ir_intf);

		else if (userInput.compare(0, 3, "spo") == 0)

			setOffset(true, userInput.erase(0, 4));

		else if (userInput.compare(0, 3, "sso") == 0)

			setOffset(false, userInput.erase(0, 4));

		else if (userInput.compare(0, 2, "ez") == 0)

			examineRawMemory(mr_intf, OT_zonename);

		else if (userInput.compare(0, 2, "et") == 0)

			examineRawMemory(mr_intf, OT_target);

		else if (userInput.compare(0, 2, "es") == 0)

			examineRawMemory(mr_intf, OT_self);

		else if (userInput.compare(0, 2, "ew") == 0)

			examineRawMemory(mr_intf, OT_world);

		else if (userInput.compare(0, 2, "fz") == 0)

			scanForString(mr_intf, OT_zonename, 0x100000, userInput.erase(0,3));

		else if (userInput.compare(0, 2, "ft") == 0)

			scanForString(mr_intf, OT_target, 0x100000, userInput.erase(0,3));

		else if (userInput.compare(0, 2, "fs") == 0)

			scanForString(mr_intf, OT_self, 0x100000, userInput.erase(0,3));

		else if (userInput.compare(0, 2, "ps") == 0)

			processSpawn(mr_intf, OT_self);

		else if (userInput.compare(0, 2, "pt") == 0)

			processSpawn(mr_intf, OT_target);

		else if (userInput.compare(0, 2, "sp") == 0)

			showProcesses(mr_intf, userInput.erase(0,3));

		else if (userInput.compare(0, 3, "sfa") == 0)

			scanForFloatFromAddress(mr_intf, userInput.erase(0,4));

		else if (userInput.compare(0, 3, "sft") == 0)

			scanForFloatFromTarget(mr_intf, userInput.erase(0,4));

		else if (userInput.compare(0, 3, "sfs") == 0)

			scanForFloatFromSelf(mr_intf, userInput.erase(0,4));

		else if (userInput.compare(0, 3, "sfu") == 0)

			scanForUINTFromSelf(mr_intf, 0x20000, userInput.erase(0,4));

		else if (userInput.compare(0, 3, "sfw") == 0)

			scanForWorldFromDate(mr_intf, OT_world, 0x100000, userInput.erase(0,4));

		else if (userInput.compare(0, 2, "sg") == 0)

			scanForString(mr_intf, OT_ground, 0x100000, "IT");

		else if (userInput.compare(0, 2, "ws") == 0)

			walkSpawnList(mr_intf, OT_self, true);

		else if (userInput.compare(0, 2, "wt") == 0)

			walkSpawnList(mr_intf, OT_target, true);

		else if (userInput.compare(0, 2, "vs") == 0)

			walkSpawnList(mr_intf, OT_self, false);

		else if (userInput.compare(0, 2, "vt") == 0)

			walkSpawnList(mr_intf, OT_target, false);

		else if (userInput.compare(0, 1, "x") == 0)

			break;

		else

            std::cout << " Invalid selection. Please try again." << std::endl;

        std::cout << "    ?) display main menu" << std::endl;

	}

}



void Debugger::displayCurrentOffsets()

{

	int i;


    std::cout << std::endl;

    std::cout << "     Primary Offsets" << std::endl;

    std::cout << "=========================" << std::endl;

	for (i=0; i<OT_max; i++)

	{
        std::cout.width(10);

        std::cout << std::setfill(' ') << std::right << i << ") " << ptrNames[i] << " = 0x" << std::hex << offsets[i] << std::endl;

	}


    std::cout << std::endl;

    std::cout << "    Secondary Spawn Offsets" << std::endl;

    std::cout << "===============================" << std::endl;

	for (i=0; i<spawnParser.OT_max ; i++)

	{
        std::cout.width(10);

        std::cout << std::setfill(' ') << std::right << i << ") " << spawnParser.ptrNames[i] << " = 0x" <<

            std::setw(3) << std::hex << std::setfill('0') << spawnParser.offsets[i] <<

            std::dec << " (" << spawnParser.offsets[i] << ")" << std::endl;

	}


    std::cout << std::endl;

    std::cout << "    Secondary Ground Offsets" << std::endl;

    std::cout << "===============================" << std::endl;

	for (i=0; i<itemParser.OT_max ; i++)

	{
        std::cout.width(10);

        std::cout << std::setfill(' ') << std::right << i << ") " << itemParser.offsetNames[i] << " = 0x" <<

            std::setw(3) << std::hex << std::setfill('0') << itemParser.offsets[i] <<

            std::dec << " (" << itemParser.offsets[i] << ")" << std::endl;

	}


    std::cout << std::endl;

    std::cout << "    Secondary World Offsets" << std::endl;

    std::cout << "===============================" << std::endl;

	for (i=0; i<worldParser.OT_max ; i++)

	{
        std::cout.width(10);

        std::cout << std::setfill(' ') << std::right << i << ") " << worldParser.offsetNames[i] << " = 0x" <<

            std::setw(3) << std::hex << std::setfill('0') << worldParser.offsets[i] <<

            std::dec << " (" << worldParser.offsets[i] << ")" << std::endl;

	}

    std::cout << std::endl;
}



void Debugger::setOffset(bool primary, std::string args)

{

	int parm1Int, parm2Int, i, OT_max;

	std::string parm1String;

	std::stringstream element(args);

	bool useIndex = true;



	// Primary offsets are in this Debugger class, but Secondary offsets live in the Spawn class.

	if (primary)

		OT_max = this->OT_max;

	else

		OT_max = spawnParser.OT_max;



	// Process the input parameters as either (string + int) or (int + int)

	if (!(element >> parm1Int))

	{

		element.clear();

		if (!(element >> parm1String))

			return;

		useIndex = false;

	}

	if (!(element >> std::hex >> parm2Int))

		return;





	// If the user has input parameter 1 as a string, determine the appropriate offset index.

	if (!useIndex)

	{

		std::string ptrName;

		std::string parm1Str(parm1String);

		TO_LOWER(parm1Str);

		parm1Int = OT_max;



		for (i=0; i<OT_max; i++)

		{

			if (primary)

				ptrName = ptrNames[i];

			else

				ptrName = spawnParser.ptrNames[i];



			TO_LOWER(ptrName);



			if (parm1Str == ptrName)

			{

				parm1Int = i;

				break;

			}

		}

	}



	if (primary)

	{

		if (setOffset((offset_types)parm1Int, parm2Int))

            std::cout << " Primary offset #" << parm1Int << " (" << ptrNames[parm1Int] << ") was set to 0x" << std::hex << parm2Int << std::endl;

		else

            std::cout << " Failed to set primary offset" << std::endl;

	}

	else

	{

		if (parm1Int >= spawnParser.OT_max)

		{
            std::cout << " Failed to set secondary offset" << std::endl;

			return;

		}



		spawnParser.offsets[parm1Int] = parm2Int;

        std::cout << " Secondary offset #" << parm1Int << " (" << spawnParser.ptrNames[parm1Int] << ") was set to 0x" << std::hex << parm2Int << std::endl;

	}

}



void Debugger::examineRawMemory(MemReaderInterface* mr_intf, offset_types ot)

{

	const UINT bufSize=2048;

	char buffer[bufSize], temp[65];

	UINT pMem, index;

	int r,c;



	memset(buffer, 0, bufSize);



	// pZone is a direct pointer. All others are indirect.

	if (ot == OT_zonename)

		pMem = offsets[ot] - 0x400000 + (UINT)mr_intf->getCurrentBaseAddress();

	else

		pMem = mr_intf->extractRAWPointer(offsets[ot]);


    std::cout << " Display Raw Memory from 0x" << (pMem + 0x400000 - (UINT)mr_intf->getCurrentBaseAddress()) << " to 0x" << (pMem + bufSize  + 0x400000 - (UINT)mr_intf->getCurrentBaseAddress()) << std::endl;



	// Read the raw memory into our local buffer

	if (pMem)

	{

		if (!(mr_intf->extractToBuffer(pMem, buffer, bufSize)))

		{
            std::cout << " Failed to read memory at address 0x" << std::hex << (pMem + 0x400000 - (UINT)mr_intf->getCurrentBaseAddress()) << std::endl;

			return;

		}

	}

	else

	{
        std::cout << " Failed to obtain valid memory pointer for offset " << ptrNames[ot] << std::endl;

		return;

	}



	// Display the data

	for (r=0; r<bufSize/16; r++)

	{
        std::cout << "0x" << std::hex << std::setfill('0') << std::setw(2) << r*16 << ") ";



		// Display data in byte format

		for (c=0; c<16; c++)

		{

			index = r*16 + c;

			_itoa_s((buffer[index] & 0xFF), temp, 65, 16);

            std::cout << " " << std::setw(2) << std::setfill('0') << std::hex << temp;

			if ((c % 4) == 3)

                std::cout << " ";

		}



		// Display data in string format

        std::cout << "  ";

		for (c=0; c<16; c++)

		{

			index = r*16 + c;

			if (isalnum(buffer[index] & 0xFF))

                std::cout << buffer[index];

			else

                std::cout << ".";

		}

        std::cout << std::endl;

	}

}



void Debugger::processSpawn(MemReaderInterface* mr_intf, offset_types ot)

{

	UINT pMem;



	// pZone is a direct pointer. All others are indirect.

	if (ot == OT_zonename)

		pMem = offsets[ot] - 0x400000 + (UINT)mr_intf->getCurrentBaseAddress();

	else

		pMem = mr_intf->extractRAWPointer(offsets[ot]);



	if (pMem)

	{

		if (!(mr_intf->extractToBuffer(pMem, spawnParser.rawBuffer, spawnParser.largestOffset)))

		{
            std::cout << " Failed to read memory at address 0x" << std::hex << (pMem + 0x400000 - (UINT)mr_intf->getCurrentBaseAddress()) << std::endl;

			return;

		}

	}

	else

	{
        std::cout << " Failed to obtain valid memory pointer for offset " << ptrNames[ot] << std::endl;

		return;

	}



	// Use the existing spawn parser to fill out the Spawn::tempNetBuffer structure, which we can examine later.

	spawnParser.packNetBufferRaw(0, pMem);



	// Display the parsed data

    std::cout << " " << ptrNames[ot] << " = 0x" << std::hex << (pMem + 0x400000 - (UINT)mr_intf->getCurrentBaseAddress()) << std::endl;

	DISPLAY_SPAWN_ITEM(OT_name, name);

	DISPLAY_SPAWN_ITEM(OT_lastname, lastName);

	DISPLAY_SPAWN_ITEMI(OT_id, id);

	DISPLAY_SPAWN_ITEMI(OT_owner, owner);

	DISPLAY_SPAWN_ITEMI(OT_level, level);

	DISPLAY_SPAWN_ITEM(OT_race, race);

	DISPLAY_SPAWN_ITEMI(OT_class, _class);

	DISPLAY_SPAWN_ITEM(OT_x, x);

	DISPLAY_SPAWN_ITEM(OT_y, y);

	DISPLAY_SPAWN_ITEM(OT_z, z);

	DISPLAY_SPAWN_ITEM(OT_heading, heading);

	DISPLAY_SPAWN_ITEM(OT_speed, speed);

	DISPLAY_SPAWN_ITEMI(OT_type, type);

	DISPLAY_SPAWN_ITEMI(OT_hidden, hidden);

	DISPLAY_SPAWN_ITEM(OT_primary, primary);

	DISPLAY_SPAWN_ITEM(OT_offhand, offhand);

}



void Debugger::walkSpawnList(MemReaderInterface* mr_intf, offset_types ot, bool reverse)

{

	UINT pMem, pPrev, pNext, spawnCount;



	pMem = mr_intf->extractRAWPointer(offsets[ot]);



	// First try and get to the initial spawn entity

	if (pMem)

	{

		if (!(mr_intf->extractToBuffer(pMem, spawnParser.rawBuffer, spawnParser.largestOffset)))

		{

			std::cout << " Failed to read memory at address 0x" << std::hex << (pMem + 0x400000 - (UINT)mr_intf->getCurrentBaseAddress()) << std::endl;

			return;

		}

	}

	else

	{

		std::cout << " Failed to obtain valid memory pointer for offset " << ptrNames[ot] << std::endl;

		return;

	}



	spawnCount = 0;



	if (reverse)

		std::cout << " Walking spawnlist in reverse." << std::endl;

	else

		std::cout << " Walking spawnlist forward." << std::endl;



	do

	{

		// At this point we appear to have located a possible valid spawn entity. We will attempt to walk the list in reverse.

		// Use the existing spawn parser to fill out the Spawn::tempNetBuffer structure. We need the name of the spawn.

		spawnParser.packNetBufferRaw(0, pMem);



		// We also need the Prev ptr values which are in Spawn::rawBuffer

		pPrev = spawnParser.extractPrevPointer();

		pNext = spawnParser.extractNextPointer();

		spawnCount++;



		// Display a small amount of information about this spawn

		std::cout << "    -----------------------------------" << std::endl;

		DISPLAY_SPAWN_ITEM(OT_name, name);

		DISPLAY_SPAWN_ITEMI(OT_id, id);

		std::cout << "    " << spawnParser.ptrNames[spawnParser.OT_prev] << " -> 0x" << std::hex << pPrev << std::endl;

		std::cout << "    " << spawnParser.ptrNames[spawnParser.OT_next] << " -> 0x" << std::hex << pNext << std::endl;



		// Walk up the list until we reach the beginning

		if (( reverse && pPrev) || (!reverse && pNext))

		{

			if (reverse)

				pMem = pPrev;

			else

				pMem = pNext;



			if (!(mr_intf->extractToBuffer(pMem, spawnParser.rawBuffer, spawnParser.largestOffset)))

			{

				if (spawnCount == 0)

					std::cout << " Failed to read memory at address 0x" << std::hex << (pMem + 0x400000 - (UINT)mr_intf->getCurrentBaseAddress()) << std::endl;

				pMem = 0;

			}

		}

	}

	while ( ((!reverse && pNext) || ( reverse && pPrev)) && (spawnCount < 1000));



	std::cout << " Discovered " << std::dec << spawnCount << " spawn entities during the walk." << std::endl;



	if (reverse)

		scanForPtr(mr_intf, pMem, 0x800000, 0x800000);

}



void Debugger::scanForPtr(MemReaderInterface* mr_intf, UINT pSearch, UINT pStart, UINT size)

{

	UINT pMem, pExtracted;



	if (!pStart)

		return;



	std::cout << " Scanning for 0x" << std::hex << pSearch << " from 0x" << (pStart + 0x400000 - (UINT)mr_intf->getCurrentBaseAddress()) << " to 0x" << (pStart+size + 0x400000 - (UINT)mr_intf->getCurrentBaseAddress()) << std::endl;



	for (pMem=pStart; pMem<(pStart+size); pMem+=1)

	{

		if (!(mr_intf->extractToBuffer(pMem, (char*) &pExtracted, sizeof(pExtracted))))

		{

			//cout << " Failed to read memory at address 0x" << hex << pMem << endl;

			//return;

			continue;

		}

		if (pSearch == pExtracted)

		{

			std::cout << " Pointer match found for 0x" << std::hex << pSearch << " at 0x" << (pMem + 0x400000 - (UINT)mr_intf->getCurrentBaseAddress()) << std::endl;

		}

	}

}



void Debugger::scanForString(MemReaderInterface* mr_intf, offset_types ot, UINT size, std::string searchStr)

{

	UINT pMem, pStart, pDeepMem, pDeepMem2, nameOffset;

	char buffer[100];

	std::string spawnName, itemName;

	if ( (offsets[ot] == 0) || (size == 0) || searchStr.length() == 0)

	{

		std::cout << " Error: '" << searchStr << "' appears to be an invalid search string." << std::endl;

		return;

	}



	pStart = offsets[ot] - 4*size - 0x400000 + (UINT)mr_intf->getCurrentBaseAddress();



	if (ot == OT_ground)

		nameOffset = itemParser.offsets[itemParser.OT_name];

	else

		nameOffset = spawnParser.offsets[spawnParser.OT_name];



	std::cout << " Scanning for '" << searchStr << "' from 0x" << std::hex << pStart << " to 0x" << (pStart+8*size) << std::endl;



	for (pMem=pStart; pMem<(pStart+size*8);)

	{

		// The zonename search is a little different

		if (ot == OT_zonename)

		{

			if (!(mr_intf->extractToBuffer(pMem, buffer, 1)))

			{

				//cout << " Failed to read memory at address 0x" << hex << pMem << endl;

				//return;

				pMem += 1;

				continue;

			}

			if (searchStr[0] == buffer[0])

			{

				mr_intf->extractToBuffer(pMem, buffer, 30);

				if (searchStr == buffer)

				{

					std::cout << " Pointer match found at 0x" << std::hex << (pMem + 0x400000 - (UINT)mr_intf->getCurrentBaseAddress()) << std::endl;

				}

			}

			pMem += 1;

		}

		else if ((ot == OT_target) || (ot == OT_self))

		{

			// When seaching for spawn names, we have an additional pointer to go thru first.

			if (!(pDeepMem = mr_intf->extractPointer(pMem)))

			{

				//cout << " Failed to read memory at address 0x" << hex << pMem << endl;

				//return;

				pMem += 4;

				continue;

			}

			if (pDeepMem > pMem)

			{

				spawnName = mr_intf->extractString(pDeepMem + nameOffset);

				if (searchStr == spawnName)

				{

					std::cout << " Pointer match found at 0x" << (pMem + 0x400000 - (UINT)mr_intf->getCurrentBaseAddress()) << std::endl;

				}

			}

			pMem += 4;

		}

		else if (ot == OT_ground)

		{

			// When seaching for ground items, we have an additional pointer to go thru first.

			if (!(pDeepMem2 = mr_intf->extractPointer(pMem)))

			{

				//cout << " Failed to read memory at address 0x" << hex << pMem << endl;

				//return;

				pMem += 4;

				continue;

			}
			if (pDeepMem2 > pMem)

			{

				itemName = mr_intf->extractString(pDeepMem2 + nameOffset);

				if ( itemName.compare(0,2,searchStr) == 0)

				{

					std::cout << " Pointer match found at 0x" << (pMem + 0x400000 - (UINT)mr_intf->getCurrentBaseAddress()) << ". Full string is " << itemName << std::endl;

				}

			}

			if (!(pDeepMem = mr_intf->extractPointer(pDeepMem2)))

			{

				//cout << " Failed to read memory at address 0x" << hex << pMem << endl;

				//return;

				pMem += 4;

				continue;

			}



			if (pDeepMem > pMem)

			{

				itemName = mr_intf->extractString(pDeepMem + nameOffset);

				if ( itemName.compare(0,2,searchStr) == 0)

				{

					std::cout << " Pointer match found at 0x" << (pMem + 0x400000 - (UINT)mr_intf->getCurrentBaseAddress()) << ". Full string is " << itemName << std::endl;

				}

			}

			pMem += 4;

		}

	}

}



void Debugger::showProcesses(MemReaderInterface* mr_intf, std::string processName)

{

	if ( processName == "" )

		processName = "eqgame";



	// Show all possible matching process information

	mr_intf->openFirstProcess(processName, true);

	while (mr_intf->openNextProcess(processName, true)){};

}



void Debugger::scanForFloatFromTarget(MemReaderInterface* mr_intf, std::string args)

{

	UINT pStart = mr_intf->extractPointer(offsets[OT_target]);

	scanForFloat(mr_intf, args, pStart, false);

}

void Debugger::scanForFloatFromSelf(MemReaderInterface* mr_intf, std::string args)

{

	UINT pStart = mr_intf->extractPointer(offsets[OT_self]);

	scanForFloat(mr_intf, args, pStart, false);

}

void Debugger::scanForUINTFromSelf(MemReaderInterface* mr_intf, UINT size, std::string args)

{

	UINT pStart;

	pStart = mr_intf->extractPointer(offsets[OT_self]);

	scanForUINT(mr_intf, pStart, size, args);

}

void Debugger::scanForFloatFromAddress(MemReaderInterface* mr_intf, std::string args)

{

	scanForFloat(mr_intf, args, 0, true);

}



void Debugger::scanForFloat(MemReaderInterface* mr_intf, std::string args, UINT pStart, bool yankPstart)

{

	const int INVALID = -100000;

	float xFind, yFind, zFind;

	int xTemp, yTemp, zTemp;

	std::vector<std::string> tokens;

	UINT pFloat, pEnd;

	bool xMatch, yMatch, zMatch;

	bool sCheck, dCheck, tCheck;



	// First get our x/y/z values if given

	xFind = yFind = zFind = INVALID;

	sCheck = dCheck = tCheck = false;



	switch ( tokenizeString(args, tokens) )

	{

		case 4:

			if (yankPstart) {

				pStart = strtol(tokens[3].c_str(), NULL, 16);
				pStart = pStart - 0x400000 + (UINT)mr_intf->getCurrentBaseAddress();

			}
			xFind = (float) atof(tokens[0].c_str());

			yFind = (float) atof(tokens[1].c_str());

			zFind = (float) atof(tokens[2].c_str());

			tCheck = true;

			break;



		case 3:

			xFind = (float) atof(tokens[0].c_str());

			yFind = (float) atof(tokens[1].c_str());

			if (yankPstart)

			{

				pStart = atoi(tokens[2].c_str());
				pStart = pStart - 0x400000 + (UINT)mr_intf->getCurrentBaseAddress();


				dCheck = true;

			}

			else

			{

				zFind = (float) atof(tokens[2].c_str());

				tCheck = true;

			}

			break;



		case 2:

			xFind = (float) atof(tokens[0].c_str());

			if (yankPstart)

			{

				pStart = atoi(tokens[1].c_str());
				pStart = pStart - 0x400000 + (UINT)mr_intf->getCurrentBaseAddress();

				sCheck = true;

			}

			else

			{

				yFind = (float) atof(tokens[1].c_str());

				dCheck = true;

			}

			break;



		case 1:

			xFind = (float) atof(tokens[0].c_str());

			sCheck = true;

			break;

		default:

			break;

	}



	if (pStart == 0)

		return;



	// For target searches, limit search to 4K. Otherwise scan 64M

	if (yankPstart)

		pEnd = pStart + 0x4000000;

	else

		pEnd = pStart + 0x1000;



	// First try and get to the initial spawn entity

	for ( pFloat = pStart; pFloat < pEnd; pFloat += 4 )

	{

		xMatch = yMatch = zMatch = false;

		xTemp = yTemp = zTemp = INVALID;



		if (sCheck || dCheck || tCheck)

		{

			xTemp = (int) mr_intf->extractFloat(pFloat);

			if (fabs(xTemp - xFind) < 20)

				xMatch = true;

		}



		if (dCheck || tCheck)

		{

			yTemp = (int) mr_intf->extractFloat(pFloat + 4);

			if (fabs(yTemp - yFind) < 20)

				yMatch = true;

		}



		if (tCheck)

		{

			zTemp = (int) mr_intf->extractFloat(pFloat + 8);

			if (fabs(zTemp - zFind) < 20)

				zMatch = true;

		}



		if (sCheck)

		{

			if (xMatch)

				std::cout << std::hex << "  X match found at offset 0x" << (pFloat - pStart + 0x400000 - (UINT)mr_intf->getCurrentBaseAddress()) << std::endl;

		}



		if (dCheck)

		{

			if (xMatch && yMatch)

				std::cout << std::hex << "  X,Y match found at offset 0x" << (pFloat - pStart + 0x400000 - (UINT)mr_intf->getCurrentBaseAddress()) << ", 0x" << (pFloat - pStart + 0x400000 - (UINT)mr_intf->getCurrentBaseAddress()) + 4

					 << std::dec << " (" << xTemp << "," << yTemp << ")" << std::endl;

		}



		if (tCheck)

		{

			if (xMatch && yMatch && zMatch)

				std::cout << std::hex << "  X,Y,Z match found at offset 0x" << (pFloat - pStart + 0x400000 - (UINT)mr_intf->getCurrentBaseAddress()) << ", 0x" << (pFloat - pStart + 0x400000 - (UINT)mr_intf->getCurrentBaseAddress()) + 4

					 << ", 0x" << (pFloat - pStart + 0x400000 - (UINT)mr_intf->getCurrentBaseAddress()) + 8 << std::dec << " (" << xTemp << "," << yTemp << "," << zTemp << ")" << std::endl;

		}

	}

}



void Debugger::scanForWorldFromDate(MemReaderInterface* mr_intf, offset_types ot, UINT size, std::string args)

{

	UINT yFind, yTemp;

	BYTE mFind;

	BYTE dFind;

	BYTE dTemp, mTemp;

	UINT pMem, pStart, pDeepMem;

	UINT dayOffset, monthOffset, yearOffset;

	std::vector<std::string> tokens;



	if ( (offsets[ot] == 0) || (size == 0) || args.length() == 0)

	{

		std::cout << "    Error: '" << args << "' appears to be an invalid date." << std::endl;
		std::cout << "    Proper usage - sfw mm/dd/yyyy" << std::endl;
		std::cout << "    Get date from Game Time using /time" << std::endl;

		return;

	}




	dayOffset = worldParser.offsets[worldParser.OT_day];

	monthOffset = worldParser.offsets[worldParser.OT_month];

	yearOffset = worldParser.offsets[worldParser.OT_year];



	// First get our month/day/year values if given

	mFind = dFind = yFind = 0;



	if ( tokenizeDate(args, tokens) != 3)

	{

		std::cout << "    Incomplete Date.  Proper usage - sfw mm/dd/yyyy" << std::endl;
		std::cout << "    Get date from Game Time using /time" << std::endl;

		return;

	}

	mFind = (BYTE) atol(tokens[0].c_str());

	dFind = (BYTE) atol(tokens[1].c_str());

	yFind = (UINT) atol(tokens[2].c_str());



	if (((int) mFind > 12) || ((int)dFind > 31))

	{

		std::cout << "    Bad Date (mm/dd/yyyy): Limit mm to max of 12 and dd to max of 31" << std::endl;

		return;

	}

	pStart = offsets[ot] - 2* size;


	if (pStart == 0)

		return;



	std::cout << " Scanning for '" << args << "' from 0x" << std::hex << pStart << " to 0x" << (pStart+4*size) << std::endl;


	pStart = pStart - 0x400000 + (UINT)mr_intf->getCurrentBaseAddress();

	for (pMem=pStart; pMem<(pStart+size*4);)

	{

		// We have an additional pointer to go thru first.

		if (!(pDeepMem = mr_intf->extractPointer(pMem)))

		{

			//cout << " Failed to read memory at address 0x" << hex << pMem << endl;

			//return;

			pMem += 4;

			continue;

		}

		if (pDeepMem > pMem)

		{

			dTemp = mr_intf->extractBYTE(pDeepMem + dayOffset);

			mTemp = mr_intf->extractBYTE(pDeepMem + monthOffset);

			yTemp = mr_intf->extractUINT(pDeepMem + yearOffset);



			if (dTemp == dFind && mTemp == mFind && yTemp == yFind)

			{

				std::cout << std::hex << "  Date match found at offset 0x" << (pMem + 0x400000 - (UINT)mr_intf->getCurrentBaseAddress()) <<

				std::dec << " (" << (int) mTemp << "/" << (int) dTemp << "/" << (int) yTemp << ")" << std::endl;

			}

		}

		pMem += 4;

	}

}

void Debugger::scanForUINT(MemReaderInterface* mr_intf, UINT pStart, UINT size, std::string args)

{

	UINT Temp;

	UINT Find;

	UINT pMem;

	std::vector<std::string> tokens;

	if ( (pStart == 0) || args.length() == 0)

	{

		std::cout << "    Error: '" << args << "' appears to be an invalid value." << std::endl;
		std::cout << "    Proper usage - sfu number" << std::endl;

		return;

	}



	if ( tokenizeString(args, tokens) != 1)

	{

		std::cout << "    Error: '" << args << "' appears to be an invalid value." << std::endl;
		std::cout << "    Proper usage - sfu number" << std::endl;

		return;

	}

	Find = (UINT) atol(tokens[0].c_str());


	if (pStart == 0)

		return;



	std::cout << " Scanning for '" << args << "' from 0x" << std::hex << pStart << " to 0x" << (pStart+4*size) << std::endl;


	pStart = pStart - 0x400000 + (UINT)mr_intf->getCurrentBaseAddress();

	for (pMem=pStart; pMem<(pStart+size*2);)

	{


		Temp = mr_intf->extractUINT(pMem);

		if (Temp == Find)

		{
			std::cout << std::dec << Find << "  match found at offset 0x" << (pMem - pStart + 0x400000 - (UINT)mr_intf->getCurrentBaseAddress()) << std::endl;

		}

		pMem += 1;

	}

}




int Debugger::tokenizeString(std::string input, std::vector<std::string>& tokens)

{

	std::string::size_type from = 0;

	std::string::size_type to = input.find(",", from);



	while ( to != std::string::npos )

	{

		tokens.push_back(input.substr(from, to-from));

        from = to + 1;

        to = input.find(",", from );

	}

	tokens.push_back(input.substr(from, input.length()));

	return tokens.size();

}



int Debugger::tokenizeDate(std::string input, std::vector<std::string>& tokens)

{

	std::string::size_type from = 0;

	std::string::size_type to = input.find("/", from);



	while ( to != std::string::npos )

	{

		tokens.push_back(input.substr(from, to-from));

        from = to + 1;

        to = input.find("/", from );

	}

	tokens.push_back(input.substr(from, input.length()));

	return tokens.size();

}