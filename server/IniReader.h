/*==============================================================================

	Copyright (C) 2006-2013  All developers at http://sourceforge.net/projects/seq

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  ==============================================================================*/

#pragma once

#include "Common.h"

class IniReaderInterface
{
public:
	virtual void openFile(std::string filename) = 0;
	virtual void openConfigFile(std::string filename) = 0;
	virtual std::string readStringEntry(std::string section, std::string entry, bool config = false) = 0;
	virtual long readIntegerEntry(std::string section, std::string entry, bool config = false) = 0;
	virtual bool writeStringEntry(std::string section, std::string entry, std::string value, bool config = false) = 0;
	virtual std::string readEscapeStrings(std::string section, std::string entry) = 0;
};

class IniReader : public IniReaderInterface
{
private:
	std::string filename;
	std::string configfilename;
	_TCHAR buffer[255];
	bool StartMinimized;

	void SetStartMinimized(bool value) { StartMinimized = value; }

public:
	IniReader();

    ~IniReader();

    void openFile(std::string filename);
	void openConfigFile(std::string filename);
	std::string readStringEntry(std::string section, std::string entry, bool config = false);
	std::string readEscapeStrings(std::string section, std::string entry);
	long readIntegerEntry(std::string section, std::string entry, bool config = false);
	bool writeStringEntry(std::string section, std::string entry, std::string value, bool config = false);
	std::string GetPatchDate();
	std::string patchDate;

    bool GetStartMinimized() { return StartMinimized; }
	void ToggleStartMinimized();
};

